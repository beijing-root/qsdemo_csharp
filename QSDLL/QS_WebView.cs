﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Timers;

namespace QSDLL
{
    public class QS_WebView : IDisposable
    {
        // 定义回调变量
        private WndProcCallback m_qsWndProcCallback = null;
        private qsPaintUpdatedCallback m_qsPaintUpdatedCallback = null;
        private qsPaintBitUpdatedCallback m_qsPaintBitUpdatedCallback = null;
        private qsOnBlinkThreadInitCallback m_qsBlinkThreadInitCallback = null;
        private qsOnGetPdfPageDataCallback m_qsGetPdfPageDataCallback = null;
        private qsRunJsCallback m_qsRunJsCallback = null;
        private qsJsQueryCallback m_qsJsQueryCallback = null;
        private qsTitleChangedCallback m_qsTitleChangedCallback = null;
        private qsMouseOverUrlChangedCallback m_qsMouseOverUrlChangedCallback = null;
        private qsUrlChangedCallback m_qsUrlChangedCallback = null;
        private qsUrlChangedCallback2 m_qsUrlChangedCallback2 = null;
        private qsAlertBoxCallback m_qsAlertBoxCallback = null;
        private qsConfirmBoxCallback m_qsConfirmBoxCallback = null;
        private qsPromptBoxCallback m_qsPromptBoxCallback = null;
        private qsNavigationCallback m_qsNavigationCallback = null;
        private qsCreateViewCallback m_qsCreateViewCallback = null;
        private qsDocumentReadyCallback m_qsDocumentReadyCallback = null;
        private qsCloseCallback m_qsCloseCallback = null;
        private qsDestroyCallback m_qsDestroyCallback = null;
        private qsOnShowDevtoolsCallback m_qsShowDevtoolsCallback = null;
        private qsDidCreateScriptContextCallback m_qsDidCreateScriptContextCallback = null;
        private qsGetPluginListCallback m_qsGetPluginListCallback = null;
        private qsLoadingFinishCallback m_qsLoadingFinishCallback = null;
        private qsDownloadCallback m_qsDownloadCallback = null;
        private qsConsoleCallback m_qsConsoleCallback = null;
        private qsLoadUrlBeginCallback m_qsLoadUrlBeginCallback = null;
        private qsLoadUrlEndCallback m_qsLoadUrlEndCallback = null;
        private qsLoadUrlFailCallback m_qsLoadUrlFailCallback = null;
        private qsWillReleaseScriptContextCallback m_qsWillReleaseScriptContextCallback = null;
        private qsNetResponseCallback m_qsNetResponseCallback = null;
        private qsNetGetFaviconCallback m_qsNetGetFaviconCallback = null;
        private qsCanGoBackForwardCallback m_qsCanGoBackForwardCallback = null;
        private qsGetCookieCallback m_qsGetCookieCallback = null;
        private qsGetSourceCallback m_qsGetSourceCallback = null;
        private qsGetContentAsMarkupCallback m_qsGetContentAsMarkupCallback = null;
        private qsOnUrlRequestWillRedirectCallback m_qsUrlRequestWillRedirectCallback = null;
        private qsOnUrlRequestDidReceiveResponseCallback m_qsUrlRequestDidReceiveResponseCallback = null;
        private qsOnUrlRequestDidReceiveDataCallback m_qsUrlRequestDidReceiveDataCallback = null;
        private qsOnUrlRequestDidFailCallback m_qsUrlRequestDidFailCallback = null;
        private qsOnUrlRequestDidFinishLoadingCallback m_qsUrlRequestDidFinishLoadingCallback = null;
        private qsNetJobDataRecvCallback m_qsNetJobDataRecvCallback = null;
        private qsNetJobDataFinishCallback m_qsNetJobDataFinishCallback = null;
        private qsPopupDialogSaveNameCallback m_qsPopupDialogSaveNameCallback = null;
        private qsDownloadInBlinkThreadCallback m_qsDownloadInBlinkThreadCallback = null;
        private qsPrintPdfDataCallback m_qsPrintPdfDataCallback = null;
        private qsPrintBitmapCallback m_qsPrintBitmapCallback = null;
        private qsWindowClosingCallback m_qsWindowClosingCallback = null;
        private qsWindowDestroyCallback m_qsWindowDestroyCallback = null;
        private qsDraggableRegionsChangedCallback m_qsDraggableRegionsChangedCallback = null;
        private qsPrintingCallback m_qsPrintingCallback = null;
        private qsImageBufferToDataURLCallback m_qsImageBufferToDataUrlCallback = null;
        private qsOnScreenshotCallback m_qsScreenshotCallback = null;
        private qsOnCallUiThreadCallback m_qsCallUiThreadCallback = null;


        // 定义事件句柄
        private event EventHandler<WindowProcEventArgs> m_qsWindowProcHandler = null;
        private event EventHandler<PaintBitUpdatedEventArgs> m_qsPaintBitUpdatedHandler = null;
        private event EventHandler<BlinkThreadInitEventArgs> m_qsBlinkThreadInitHandler = null;
        private event EventHandler<GetPdfPageDataEventArgs> m_qsGetPdfPageDataHandler = null;
        private event EventHandler<RunJsEventArgs> m_qsRunJsHandler = null;
        private event EventHandler<JsQueryEventArgs> m_qsJsQueryHandler = null;
        private event EventHandler<TitleChangedEventArgs> m_qsTitleChangedHandler = null;
        private event EventHandler<MouseOverUrlChangedEventArgs> m_qsMouseOverUrlChangedHandler = null;
        private event EventHandler<UrlChangedEventArgs> m_qsUrlChangedHandler = null;
        private event EventHandler<UrlChangedEventArgs2> m_qsUrlChangedHandler2 = null;
        private event EventHandler<AlertBoxEventArgs> m_qsAlertBoxHandler = null;
        private event EventHandler<ConfirmBoxEventArgs> m_qsConfirmBoxHandler = null;
        private event EventHandler<PromptBoxEventArgs> m_qsPromptBoxHandler = null;
        private event EventHandler<NavigationEventArgs> m_qsNavigationHandler = null;
        private event EventHandler<CreateViewEventArgs> m_qsCreateViewHandler = null;
        private event EventHandler<DocumentReadyEventArgs> m_qsDocumentReadyHandler = null;
        private event EventHandler<CloseEventArgs> m_qsCloseHandler = null;
        private event EventHandler<DestroyEventArgs> m_qsDestroyHandler = null;
        private event EventHandler<ShowDevtoolsEventArgs> m_qsShowDevtoolsHandler = null;
        private event EventHandler<DidCreateScriptContextEventArgs> m_qsDidCreateScriptContextHandler = null;
        private event EventHandler<GetPluginListEventArgs> m_qsGetPluginListHandler = null;
        private event EventHandler<LoadingFinishEventArgs> m_qsLoadingFinishHandler = null;
        private event EventHandler<DownloadEventArgs> m_qsDownloadHandler = null;
        private event EventHandler<ConsoleEventArgs> m_qsConsoleHandler = null;
        private event EventHandler<LoadUrlBeginEventArgs> m_qsLoadUrlBeginHandler = null;
        private event EventHandler<LoadUrlEndEventArgs> m_qsLoadUrlEndHandler = null;
        private event EventHandler<LoadUrlFailEventArgs> m_qsLoadUrlFailHandler = null;
        private event EventHandler<WillReleaseScriptContextEventArgs> m_qsWillReleaseScriptContextHandler = null;
        private event EventHandler<NetResponseEventArgs> m_qsNetResponseHandler = null;
        private event EventHandler<NetGetFaviconEventArgs> m_qsNetGetFaviconHandler = null;
        private event EventHandler<CanGoBackForwardEventArgs> m_qsCanGoBackForwardHandler = null;
        private event EventHandler<GetCookieEventArgs> m_qsGetCookieHandler = null;
        private event EventHandler<GetSourceEventArgs> m_qsGetSourceHandler = null;
        private event EventHandler<GetContentAsMarkupEventArgs> m_qsGetContentAsMarkupHandler = null;
        private event EventHandler<UrlRequestWillRedirectEventArgs> m_qsUrlRequestWillRedirectHandler = null;
        private event EventHandler<UrlRequestDidReceiveResponseEventArgs> m_qsUrlRequestDidReceiveResponseHandler = null;
        private event EventHandler<UrlRequestDidReceiveDataEventArgs> m_qsUrlRequestDidReceiveDataHandler = null;
        private event EventHandler<UrlRequestDidFailEventArgs> m_qsUrlRequestDidFailHandler = null;
        private event EventHandler<UrlRequestDidFinishLoadingEventArgs> m_qsUrlRequestDidFinishLoadingHandler = null;
        private event EventHandler<NetJobDataRecvEventArgs> m_qsNetJobDataRecvHandler = null;
        private event EventHandler<NetJobDataFinishEventArgs> m_qsNetJobDataFinishHandler = null;
        private event EventHandler<PopupDialogSaveNameEventArgs> m_qsPopupDialogSaveNameHandler = null;
        private event EventHandler<DownloadInBlinkThreadEventArgs> m_qsDownloadInBlinkThreadHandler = null;
        private event EventHandler<PrintPdfDataEventArgs> m_qsPrintPdfDataHandler = null;
        private event EventHandler<PrintBitmapEventArgs> m_qsPrintBitmapHandler = null;
        private event EventHandler<WindowClosingEventArgs> m_qsWindowClosingHandler = null;
        private event EventHandler<WindowDestroyEventArgs> m_qsWindowDestroyHandler = null;
        private event EventHandler<DraggableRegionsChangedEventArgs> m_qsDraggableRegionsChangedHandler = null;
        private event EventHandler<PrintingEventArgs> m_qsPrintingHandler = null;
        private event EventHandler<ImageBufferToDataUrlEventArgs> m_qsImageBufferToDataUrlHandler = null;
        private event EventHandler<ScreenshotEventArgs> m_qsScreenshotHandler = null;
        private event EventHandler<CallUiThreadEventArgs> m_qsCallUiThreadHandler = null;



        #region --------------------------- 各种事件参数 ---------------------------

        public class MiniblinkEventArgs : EventArgs
        {
            public MiniblinkEventArgs(IntPtr webView)
            {
                Handle = webView;
            }

            public IntPtr Handle { get; }
        }

        public class WindowProcEventArgs : EventArgs
        {
            public WindowProcEventArgs(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam)
            {
                Handle = hWnd;
                iMsg = msg;
                this.wParam = wParam;
                this.lParam = lParam;
            }

            public IntPtr Handle { get; }
            public int iMsg { get; }
            public IntPtr wParam { get; }
            public IntPtr lParam { get; }
            public IntPtr Result { get; set; }

            /// <summary>
            /// 如果为 true 则会返回 Result ，false 则返回默认值
            /// </summary>
            public bool bHand { get; set; }
        }

        public class PaintBitUpdatedEventArgs : MiniblinkEventArgs
        {
            public PaintBitUpdatedEventArgs(IntPtr webView, IntPtr param, IntPtr buffer, IntPtr rect, int width, int height) : base(webView)
            {
                byteBuffer = buffer.UTF8PtrToByte();
                Rect = (qsRect)rect.UTF8PtrToStruct(typeof(qsRect));
                iWidth = width;
                iHeight = height;
            }

            public byte[] byteBuffer { get; }
            public qsRect Rect { get; }
            public int iWidth { get; }
            public int iHeight { get; }
        }

        public class BlinkThreadInitEventArgs : EventArgs
        {
            public BlinkThreadInitEventArgs(IntPtr param)
            {

            }
        }

        public class GetPdfPageDataEventArgs : MiniblinkEventArgs
        {
            public GetPdfPageDataEventArgs(IntPtr webView, IntPtr param, IntPtr data, ulong size) : base(webView)
            {
                PdfData = data.UTF8PtrToByte();
            }

            public byte[] PdfData { get; }
        }

        public class RunJsEventArgs : MiniblinkEventArgs
        {
            public RunJsEventArgs(IntPtr webView, IntPtr param, IntPtr es, ulong v) : base(webView)
            {
                fRet = QS_API.qsJsToDouble(es, v);

                IntPtr ptrRet = QS_API.qsJsToString(es, v);
                strRet = ptrRet.UTF8PtrToStr();

                bRet = QS_API.qsJsToBoolean(es, v) == 1 ? true : false;

                strParam = param.UTF8PtrToStr();
            }

            public double fRet { get; }
            public string strRet { get; }
            public bool bRet { get; }
            public string strParam { get; }
        }

        public class JsQueryEventArgs : MiniblinkEventArgs
        {
            public JsQueryEventArgs(IntPtr webView, IntPtr param, IntPtr es, ulong queryId, int customMsg, IntPtr request) : base(webView)
            {
                iCustomMsg = customMsg;
                strRequest = request.UTF8PtrToStr();
                ulQueryId = queryId;
            }

            public int iCustomMsg { get; }
            public string strRequest { get; }
            public ulong ulQueryId { get; }
        }

        public class TitleChangedEventArgs : MiniblinkEventArgs
        {
            public TitleChangedEventArgs(IntPtr webView, IntPtr param, IntPtr title) : base(webView)
            {
                strTitle = title.UTF8PtrToStr();
            }

            public string strTitle { get; }
        }

        public class MouseOverUrlChangedEventArgs : MiniblinkEventArgs
        {
            public MouseOverUrlChangedEventArgs(IntPtr webView, IntPtr param, IntPtr url) : base(webView)
            {
                strUrl = url.UTF8PtrToStr();
            }

            public string strUrl { get; }
        }

        public class UrlChangedEventArgs : MiniblinkEventArgs
        {
            public UrlChangedEventArgs(IntPtr webView, IntPtr param, IntPtr url, int canGoBack, int canGoForward) : base(webView)
            {
                strUrl = url.UTF8PtrToStr();
                iCanGoBack = canGoBack;
                iCanGoForward = canGoForward;
            }

            public string strUrl { get; }
            public int iCanGoBack { get; }
            public int iCanGoForward { get; }
        }

        public class UrlChangedEventArgs2 : MiniblinkEventArgs
        {
            public UrlChangedEventArgs2(IntPtr webView, IntPtr param, IntPtr frameId, IntPtr url) : base(webView)
            {
                ptrFrameId = frameId;
                strUrl = url.UTF8PtrToStr();
            }

            public IntPtr ptrFrameId { get; }
            public string strUrl { get; }
        }

        public class AlertBoxEventArgs : MiniblinkEventArgs
        {
            public AlertBoxEventArgs(IntPtr webView, IntPtr param, IntPtr msg) : base(webView)
            {
                strMsg = msg.UTF8PtrToStr();
            }

            public string strMsg { get; }
        }

        public class ConfirmBoxEventArgs : MiniblinkEventArgs
        {
            public ConfirmBoxEventArgs(IntPtr webView, IntPtr param, IntPtr msg) : base(webView)
            {
                strMsg = msg.UTF8PtrToStr();
                iRet = msg == IntPtr.Zero ? 0 : 1;
            }

            public string strMsg { get; set; }
            public int iRet { get; }
        }

        public class PromptBoxEventArgs : MiniblinkEventArgs
        {
            public PromptBoxEventArgs(IntPtr webView, IntPtr param, IntPtr msg, IntPtr defaultResult) : base(webView)
            {
                strMsg = msg.UTF8PtrToStr();
                strResult = defaultResult.UTF8PtrToStr();
                ptrRet = msg;
            }

            public string strMsg { get; set; }
            public string strResult { get; }
            public IntPtr ptrRet { get; }
        }

        public class NavigationEventArgs : MiniblinkEventArgs
        {
            public NavigationEventArgs(IntPtr webView, IntPtr param, qsNavigationType navigationType, IntPtr url) : base(webView)
            {
                NavigationType = navigationType;
                strUrl = url.UTF8PtrToStr();
                iRet = url == IntPtr.Zero ? 0 : 1;
            }

            public qsNavigationType NavigationType { get; set; }
            public string strUrl { get; }
            public int iRet { get; }
        }

        public class CreateViewEventArgs : MiniblinkEventArgs
        {
            private IntPtr m_windowFeatures;
            public CreateViewEventArgs(IntPtr webView, IntPtr param, qsNavigationType navigationType, IntPtr url, IntPtr windowFeatures) : base(webView)
            {
                NavigationType = navigationType;
                strUrl = url.UTF8PtrToStr();
                m_windowFeatures = windowFeatures;
                NewWebViewHandle = webView;
            }

            public qsNavigationType NavigationType { get; }
            public IntPtr NewWebViewHandle { get; set; }
            public string strUrl { get; }

            public qsWindowFeatures WindowFeatures
            {
                get
                {
                    if (m_windowFeatures != IntPtr.Zero)
                    {
                        return (qsWindowFeatures)Marshal.PtrToStructure(m_windowFeatures, typeof(qsWindowFeatures));
                    }
                    else
                    {
                        return new qsWindowFeatures();
                    }
                }
            }
        }

        public class DocumentReadyEventArgs : MiniblinkEventArgs
        {
            public DocumentReadyEventArgs(IntPtr webView, IntPtr param, IntPtr frameId) : base(webView)
            {
                Frame = frameId;
            }

            public IntPtr Frame { get; }
        }

        public class CloseEventArgs : MiniblinkEventArgs
        {
            public CloseEventArgs(IntPtr webView, IntPtr param, IntPtr unuse) : base(webView)
            {

            }

            /// <summary>
            /// 1取消关闭，0不取消
            /// </summary>
            public int iCancel { get; set; }
        }

        public class DestroyEventArgs : MiniblinkEventArgs
        {
            public DestroyEventArgs(IntPtr webView, IntPtr param, IntPtr unuse) : base(webView)
            {
                iRet = 1;
            }

            public int iRet { get; }
        }

        public class ShowDevtoolsEventArgs : MiniblinkEventArgs
        {
            public ShowDevtoolsEventArgs(IntPtr webView, IntPtr param) : base(webView)
            {

            }
        }

        public class DidCreateScriptContextEventArgs : MiniblinkEventArgs
        {
            public DidCreateScriptContextEventArgs(IntPtr webView, IntPtr param, IntPtr frameId, IntPtr context, int extensionGroup, int worldId) : base(webView)
            {
                ptrFrameId = frameId;
                strContext = context.UTF8PtrToStr();
                iExtensionGroup = extensionGroup;
                iWorldId = worldId;
            }

            public IntPtr ptrFrameId { get; }
            public string strContext { get; }
            public int iExtensionGroup { get; }
            public int iWorldId { get; }
        }

        public class GetPluginListEventArgs : EventArgs
        {
            public GetPluginListEventArgs(int refresh, IntPtr pluginListBuilder, IntPtr param)
            {
                iRefresh = refresh;
            }

            public int iRefresh { get; }
        }

        public class LoadingFinishEventArgs : MiniblinkEventArgs
        {
            public LoadingFinishEventArgs(IntPtr webView, IntPtr param, IntPtr frameId, IntPtr url, qsLoadingResult result, IntPtr failedReason) : base(webView)
            {
                ptrFrameId = frameId;
                strUrl = url.UTF8PtrToStr();
                LoadingResult = result;
                strFailedReason = failedReason.UTF8PtrToStr();
            }

            public IntPtr ptrFrameId { get; }
            public string strUrl { get; }
            public qsLoadingResult LoadingResult { get; }
            public string strFailedReason { get; }
        }

        public class DownloadEventArgs : MiniblinkEventArgs
        {
            private IntPtr m_url;

            public DownloadEventArgs(IntPtr webView, IntPtr param, IntPtr frameId, IntPtr url, IntPtr downloadJob) : base(webView)
            {
                m_url = url;
            }

            public string SaveFilePath { get; set; }

            public bool Cancel { get; set; }

            public long ContentLength { get; set; }

            public string URL
            {
                get { return m_url.UTF8PtrToStr(); }
            }

            public event EventHandler<DownloadProgressEventArgs> Progress;

            public void OnProgress(DownloadProgressEventArgs e)
            {
                Progress?.Invoke(this, e);
            }

            public event EventHandler<DownloadFinishEventArgs> Finish;

            public void OnFinish(DownloadFinishEventArgs e)
            {
                Finish?.Invoke(this, e);
            }
        }

        /// <summary>
        /// 下载过程事件参数
        /// </summary>
        public class DownloadProgressEventArgs : EventArgs
        {
            public long Total { get; set; }
            public long Received { get; set; }
            public byte[] Data { get; set; }
            public bool Cancel { get; set; }
        }

        /// <summary>
        /// 下载完成事件参数
        /// </summary>
        public class DownloadFinishEventArgs : EventArgs
        {
            public Exception Error { get; set; }
            public bool IsCompleted { get; set; }
        }

        public class ConsoleEventArgs : MiniblinkEventArgs
        {
            public ConsoleEventArgs(IntPtr webView, IntPtr param, qsConsoleLevel level, IntPtr message, IntPtr sourceName, uint sourceLine, IntPtr stackTrace) : base(webView)
            {
                Level = level;
                strMessage = message.UTF8PtrToStr();
                strSourceName = sourceName.UTF8PtrToStr();
                iSourceLine = sourceLine;
                strStackTrace = stackTrace.UTF8PtrToStr();
            }

            public qsConsoleLevel Level { get; }
            public string strMessage { get; }
            public string strSourceName { get; }
            public uint iSourceLine { get; }
            public string strStackTrace { get; }
        }

        public class LoadUrlBeginEventArgs : MiniblinkEventArgs
        {
            public LoadUrlBeginEventArgs(IntPtr webView, IntPtr param, IntPtr url, IntPtr job) : base(webView)
            {
                strUrl = url.UTF8PtrToStr();
                ptrJob = job;
            }

            /// <summary>
            /// 是否取消载入，true 表示取消，该方法已经被qsNetCancelRequest所取代
            /// </summary>
            public bool bCancel { get; set; }
            public string strUrl { get; }
            public IntPtr ptrJob { get; }
        }

        public class LoadUrlEndEventArgs : MiniblinkEventArgs
        {
            public LoadUrlEndEventArgs(IntPtr webView, IntPtr param, IntPtr url, IntPtr job, IntPtr buf, int len) : base(webView)
            {
                strUrl = url.UTF8PtrToStr();
                byteData = buf.UTF8PtrToByte();
                ptrJob = job;
            }

            public string strUrl { get; }
            public byte[] byteData { get; }
            public IntPtr ptrJob { get; }
        }

        public class LoadUrlFailEventArgs : MiniblinkEventArgs
        {
            public LoadUrlFailEventArgs(IntPtr webView, IntPtr param, IntPtr url, IntPtr job) : base(webView)
            {
                strUrl = url.UTF8PtrToStr();
                ptrJob = job;
            }

            public string strUrl { get; }
            public IntPtr ptrJob { get; }
        }

        public class WillReleaseScriptContextEventArgs : MiniblinkEventArgs
        {
            public WillReleaseScriptContextEventArgs(IntPtr webView, IntPtr param, IntPtr frameId, IntPtr context, int worldId) : base(webView)
            {
                ptrFrameId = frameId;
                strContext = context.UTF8PtrToStr();
                iWorldId = worldId;
            }

            public IntPtr ptrFrameId { get; }
            public string strContext { get; }
            public int iWorldId { get; }
        }

        public class NetResponseEventArgs : MiniblinkEventArgs
        {
            public NetResponseEventArgs(IntPtr webView, IntPtr param, IntPtr url, IntPtr job) : base(webView)
            {
                strUrl = url.UTF8PtrToStr();
                iRet = url == IntPtr.Zero ? 0 : 1; ;
                ptrJob = job;
            }

            public string strUrl { get; }
            public int iRet { get; }
            public IntPtr ptrJob { get; }
        }

        public class NetGetFaviconEventArgs : MiniblinkEventArgs
        {
            public NetGetFaviconEventArgs(IntPtr webView, IntPtr param, IntPtr url, IntPtr buf) : base(webView)
            {
                strUrl = url.UTF8PtrToStr();
                byteBuf = buf.UTF8PtrToByte();
            }

            public string strUrl { get; }
            public byte[] byteBuf { get; }
        }

        public class CanGoBackForwardEventArgs : MiniblinkEventArgs
        {
            public CanGoBackForwardEventArgs(IntPtr webView, IntPtr param, qsAsynRequestState state, int b) : base(webView)
            {
                State = state;
                bRet = state == qsAsynRequestState.QS_AsynRequestStateOk ? (b == 1 ? true : false) : false;
            }

            public qsAsynRequestState State { get; }
            public bool bRet { get; }
        }

        public class GetCookieEventArgs : MiniblinkEventArgs
        {
            public GetCookieEventArgs(IntPtr webView, IntPtr param, qsAsynRequestState state, IntPtr cookie) : base(webView)
            {
                State = state;
                strCookie = State == qsAsynRequestState.QS_AsynRequestStateOk ? cookie.UTF8PtrToStr() : null; ;
            }

            public qsAsynRequestState State { get; }
            public string strCookie { get; }
        }

        public class GetSourceEventArgs : MiniblinkEventArgs
        {
            public GetSourceEventArgs(IntPtr webView, IntPtr param, IntPtr mhtml) : base(webView)
            {
                strHtmlCode = mhtml.UTF8PtrToStr();
            }

            public string strHtmlCode { get; }
        }

        public class GetContentAsMarkupEventArgs : MiniblinkEventArgs
        {
            public GetContentAsMarkupEventArgs(IntPtr webView, IntPtr param, IntPtr content, ulong size) : base(webView)
            {
                strContent = content.UTF8PtrToStr(); ;
            }

            public string strContent { get; }
        }

        public class UrlRequestWillRedirectEventArgs : MiniblinkEventArgs
        {
            public UrlRequestWillRedirectEventArgs(IntPtr webView, IntPtr param, IntPtr oldRequest, IntPtr request, IntPtr redirectResponse) : base(webView)
            {
                strOldRequest = oldRequest.UTF8PtrToStr();
                strRequest = request.UTF8PtrToStr();
                strRedirectResponse = redirectResponse.UTF8PtrToStr();
            }

            public string strOldRequest { get; }
            public string strRequest { get; }
            public string strRedirectResponse { get; }
        }

        public class UrlRequestDidReceiveResponseEventArgs : MiniblinkEventArgs
        {
            public UrlRequestDidReceiveResponseEventArgs(IntPtr webView, IntPtr param, IntPtr request, IntPtr response) : base(webView)
            {
                strRequest = request.UTF8PtrToStr();
                strResponse = response.UTF8PtrToStr();
            }

            public string strRequest { get; }
            public string strResponse { get; }
        }

        public class UrlRequestDidReceiveDataEventArgs : MiniblinkEventArgs
        {
            public UrlRequestDidReceiveDataEventArgs(IntPtr webView, IntPtr param, IntPtr request, IntPtr data, int dataLength) : base(webView)
            {
                strRequest = request.UTF8PtrToStr();
                byteData = data.UTF8PtrToByte();
            }

            public string strRequest { get; }
            public byte[] byteData { get; }
        }

        public class UrlRequestDidFailEventArgs : MiniblinkEventArgs
        {
            public UrlRequestDidFailEventArgs(IntPtr webView, IntPtr param, IntPtr request, IntPtr error) : base(webView)
            {
                strRequest = request.UTF8PtrToStr();
                strError = error.UTF8PtrToStr();
            }

            public string strRequest { get; }
            public string strError { get; }
        }

        public class UrlRequestDidFinishLoadingEventArgs : MiniblinkEventArgs
        {
            public UrlRequestDidFinishLoadingEventArgs(IntPtr webView, IntPtr param, IntPtr request, double finishTime) : base(webView)
            {
                strRequest = request.UTF8PtrToStr();
                fFinishTime = finishTime;
            }

            public string strRequest { get; }
            public double fFinishTime { get; }
        }

        public class NetJobDataRecvEventArgs : EventArgs
        {
            public NetJobDataRecvEventArgs(IntPtr ptr, IntPtr job, IntPtr data, int length)
            {
                byteData = data.UTF8PtrToByte();
                ptrJob = job;
            }

            public byte[] byteData { get; }
            public IntPtr ptrJob { get; }
        }

        public class NetJobDataFinishEventArgs : EventArgs
        {
            public NetJobDataFinishEventArgs(IntPtr ptr, IntPtr job, qsLoadingResult result)
            {
                Result = result;
                ptrJob = job;
            }

            public qsLoadingResult Result { get; }
            public IntPtr ptrJob { get; }
        }

        public class PopupDialogSaveNameEventArgs : EventArgs
        {
            public PopupDialogSaveNameEventArgs(IntPtr ptr, IntPtr filePath)
            {
                strPath = filePath.UnicodePtrToStr();
            }

            public string strPath { get; }
        }

        public class DownloadInBlinkThreadEventArgs : MiniblinkEventArgs
        {
            public DownloadInBlinkThreadEventArgs(IntPtr webView, IntPtr param, ulong expectedContentLength, IntPtr url, IntPtr mime, IntPtr disposition, IntPtr job, IntPtr dataBind) : base(webView)
            {
                iLength = expectedContentLength;
                strUrl = url.UTF8PtrToStr();
                strMime = mime.UTF8PtrToStr();
                strDisposition = disposition.UTF8PtrToStr();
                DownloadRet = expectedContentLength == 0 ? qsDownloadOpt.QS_DownloadOptCancel : qsDownloadOpt.QS_DownloadOptCacheData;
                byteData = dataBind.UTF8PtrToByte();
                ptrJob = job;
            }

            public ulong iLength { get; }
            public string strUrl { get; }
            public string strMime { get; }
            public string strDisposition { get; }
            public qsDownloadOpt DownloadRet { get; }
            public byte[] byteData { get; }
            public IntPtr ptrJob { get; }
        }

        public class PrintPdfDataEventArgs : MiniblinkEventArgs
        {
            public PrintPdfDataEventArgs(IntPtr webView, IntPtr param, IntPtr data) : base(webView)
            {
                byteData = data.UTF8PtrToByte();
            }

            public byte[] byteData { get; }
        }

        public class PrintBitmapEventArgs : MiniblinkEventArgs
        {
            public PrintBitmapEventArgs(IntPtr webView, IntPtr param, IntPtr data, ulong size) : base(webView)
            {
                byteData = data.UTF8PtrToByte();
            }

            public byte[] byteData { get; }
        }

        public class WindowClosingEventArgs : MiniblinkEventArgs
        {
            public WindowClosingEventArgs(IntPtr webView, IntPtr param) : base(webView)
            {

            }

            /// <summary>
            /// 1取消关闭，0不取消
            /// </summary>
            public int iCancel { get; set; }
        }

        public class WindowDestroyEventArgs : MiniblinkEventArgs
        {
            public WindowDestroyEventArgs(IntPtr webView, IntPtr param) : base(webView)
            {

            }
        }

        public class DraggableRegionsChangedEventArgs : MiniblinkEventArgs
        {
            public DraggableRegionsChangedEventArgs(IntPtr webView, IntPtr param, IntPtr rects, int rectCount) : base(webView)
            {
                Region = (qsDraggableRegion)rects.UTF8PtrToStruct(typeof(qsDraggableRegion));
                iRectCount = rectCount;
            }

            public qsDraggableRegion Region { get; }
            public int iRectCount { get; }
        }

        public class PrintingEventArgs : MiniblinkEventArgs
        {
            public PrintingEventArgs(IntPtr webView, IntPtr param, qsPrintintStep step, IntPtr hDC, IntPtr settings, int pageCount) : base(webView)
            {
                mStep = step;
                ptrHDC = hDC;
                Settings = (qsPrintintSettings)settings.UTF8PtrToStruct(typeof(qsPrintintSettings));
                iPageCount = pageCount;
            }

            public qsPrintintStep mStep { get; }
            public IntPtr ptrHDC { get; }
            public qsPrintintSettings Settings { get; }
            public int iPageCount { get; }
        }

        public class ImageBufferToDataUrlEventArgs : MiniblinkEventArgs
        {
            public ImageBufferToDataUrlEventArgs(IntPtr webView, IntPtr param, IntPtr data, ulong size) : base(webView)
            {
                byteData = data.UTF8PtrToByte();
            }

            public byte[] byteData { get; set; }
        }

        public class ScreenshotEventArgs : MiniblinkEventArgs
        {
            public ScreenshotEventArgs(IntPtr webView, IntPtr param, IntPtr data, ulong size) : base(webView)
            {
                byteData = data.UTF8PtrToByte();
            }

            public byte[] byteData { get; }
        }

        public class CallUiThreadEventArgs : MiniblinkEventArgs
        {
            public CallUiThreadEventArgs(IntPtr webView, IntPtr paramOnInThread) : base(webView)
            {
                strParamOnInThread = paramOnInThread.UTF8PtrToStr();
            }

            public string strParamOnInThread { get; }
        }

        #endregion

        #region --------------------------- 窗口相关事件处理函数 ---------------------------

        /// <summary>
        /// 窗口刷新事件处理函数
        /// </summary>
        /// <param name="webView">浏览器窗口</param>
        /// <param name="hWnd">窗口句柄</param>
        /// <param name="hdc">DC句柄</param>
        /// <param name="x">初始X坐标</param>
        /// <param name="y">初始Y坐标</param>
        /// <param name="cx">X坐标变化量</param>
        /// <param name="cy">Y坐标变化量</param>
        private void onPaintUpdated(IntPtr webView, IntPtr hWnd, IntPtr hdc, int x, int y, int cx, int cy)
        {
            if ((int)WinConst.WS_EX_LAYERED == ((int)WinConst.WS_EX_LAYERED & QS_Common.GetWindowLong(m_hWnd, (int)WinConst.GWL_EXSTYLE)))
            {
                RECT rectDest = new RECT();
                QS_Common.GetWindowRect(m_hWnd, ref rectDest);
                QS_Common.OffsetRect(ref rectDest, -rectDest.Left, -rectDest.Top);

                SIZE sizeDest = new SIZE(rectDest.Right - rectDest.Left, rectDest.Bottom - rectDest.Top);
                POINT pointSource = new POINT();

                BITMAP bmp = new BITMAP();
                IntPtr hBmp = QS_Common.GetCurrentObject(hdc, (int)WinConst.OBJ_BITMAP);
                QS_Common.GetObject(hBmp, Marshal.SizeOf(typeof(BITMAP)), ref bmp);

                sizeDest.cx = bmp.bmWidth;
                sizeDest.cy = bmp.bmHeight;

                IntPtr hdcScreen = QS_Common.GetDC(hWnd);

                BLENDFUNCTION blend = new BLENDFUNCTION();
                blend.BlendOp = (byte)WinConst.AC_SRC_OVER;
                blend.SourceConstantAlpha = 255;
                blend.AlphaFormat = (byte)WinConst.AC_SRC_ALPHA;

                int callOk = QS_Common.UpdateLayeredWindow(m_hWnd, hdcScreen, IntPtr.Zero, ref sizeDest, hdc, ref pointSource, 0, ref blend, (int)WinConst.ULW_ALPHA);
                if (callOk == 0)
                {
                    IntPtr hdcMemory = QS_Common.CreateCompatibleDC(hdcScreen);
                    IntPtr hbmpMemory = QS_Common.CreateCompatibleBitmap(hdcScreen, sizeDest.cx, sizeDest.cy);
                    IntPtr hbmpOld = QS_Common.SelectObject(hdcMemory, hbmpMemory);

                    QS_Common.BitBlt(hdcMemory, 0, 0, sizeDest.cx, sizeDest.cy, hdc, 0, 0, (int)WinConst.SRCCOPY | (int)WinConst.CAPTUREBLT);
                    QS_Common.BitBlt(hdc, 0, 0, sizeDest.cx, sizeDest.cy, hdcMemory, 0, 0, (int)WinConst.SRCCOPY | (int)WinConst.CAPTUREBLT);

                    callOk = QS_Common.UpdateLayeredWindow(m_hWnd, hdcScreen, IntPtr.Zero, ref sizeDest, hdcMemory, ref pointSource, 0, ref blend, (int)WinConst.ULW_ALPHA);

                    QS_Common.SelectObject(hdcMemory, hbmpOld);
                    QS_Common.DeleteObject(hbmpMemory);
                    QS_Common.DeleteDC(hdcMemory);
                }

                QS_Common.ReleaseDC(m_hWnd, hdcScreen);
            }
            else
            {
                RECT rc = new RECT(x, y, x + cx, y + cy);
                QS_Common.InvalidateRect(m_hWnd, ref rc, true);
            }
        }

        /// <summary>
        /// 窗口过程事件处理函数，主要处理各种消息
        /// </summary>
        /// <param name="hWnd">窗口句柄</param>
        /// <param name="msg">消息类型</param>
        /// <param name="wParam">消息参数1</param>
        /// <param name="lParam">消息参数2</param>
        /// <returns></returns>
        private IntPtr onWndProc(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam)
        {
            int iRet = 0;
            if (m_qsWindowProcHandler != null)
            {
                WindowProcEventArgs e = new WindowProcEventArgs(hWnd, (int)msg, wParam, lParam);
                m_qsWindowProcHandler(this, e);
                if (e.bHand)
                {
                    return e.Result;
                }
            }

            switch (msg)
            {
                case (uint)WinConst.WM_PAINT:
                    {
                        if ((int)WinConst.WS_EX_LAYERED != ((int)WinConst.WS_EX_LAYERED & (int)QS_Common.GetWindowLong(hWnd, (int)WinConst.GWL_EXSTYLE)))
                        {
                            PAINTSTRUCT ps = new PAINTSTRUCT();
                            IntPtr hdc = QS_Common.BeginPaint(hWnd, ref ps);

                            RECT rcClip = ps.rcPaint;
                            RECT rcClient = new RECT();
                            QS_Common.GetClientRect(hWnd, ref rcClient);

                            RECT rcInvalid = rcClient;
                            if (rcClip.Right != rcClip.Left && rcClip.Bottom != rcClip.Top)
                            {
                                QS_Common.IntersectRect(ref rcInvalid, ref rcClip, ref rcClient);
                            }

                            int srcX = rcInvalid.Left - rcClient.Left;
                            int srcY = rcInvalid.Top - rcClient.Top;
                            int destX = rcInvalid.Left;
                            int destY = rcInvalid.Top;
                            int width = rcInvalid.Right - rcInvalid.Left;
                            int height = rcInvalid.Bottom - rcInvalid.Top;

                            if (0 != width && 0 != height)
                            {
                                IntPtr hMbDC = QS_API.qsGetLockedViewDC(m_WebView);
                                QS_Common.BitBlt(hdc, destX, destY, width, height, hMbDC, srcX, srcY, (int)WinConst.SRCCOPY);
                            }

                            QS_Common.EndPaint(hWnd, ref ps);
                            return IntPtr.Zero;
                        }

                        break;
                    }
                case (uint)WinConst.WM_ERASEBKGND:
                    {
                        return new IntPtr(1);
                    }
                case (uint)WinConst.WM_SIZE:
                    {
                        int width = lParam.ToInt32() & 65535;
                        int height = lParam.ToInt32() >> 16;
                        QS_API.qsResize(m_WebView, width, height);
                        break;
                    }
                case (uint)WinConst.WM_KEYDOWN:
                    {
                        uint virtualKeyCode = (uint)wParam.ToInt32();
                        uint flags = 0;
                        if (((lParam.ToInt32() >> 16) & (int)WinConst.KF_REPEAT) != 0)
                        {
                            flags |= (uint)qsKeyFlags.QS_REPEAT;
                        }

                        if (((lParam.ToInt32() >> 16) & (int)WinConst.KF_EXTENDED) != 0)
                        {
                            flags |= (uint)qsKeyFlags.QS_EXTENDED;
                        }

                        iRet = QS_API.qsFireKeyDownEvent(m_WebView, virtualKeyCode, flags, 0);
                        if (iRet != 0)
                        {
                            return IntPtr.Zero;
                        }

                        break;
                    }
                case (uint)WinConst.WM_KEYUP:
                    {
                        uint virtualKeyCode = (uint)wParam.ToInt32();
                        uint flags = 0;
                        if (((lParam.ToInt32() >> 16) & (int)WinConst.KF_REPEAT) != 0)
                        {
                            flags |= (uint)qsKeyFlags.QS_REPEAT;
                        }

                        if (((lParam.ToInt32() >> 16) & (int)WinConst.KF_EXTENDED) != 0)
                        {
                            flags |= (uint)qsKeyFlags.QS_EXTENDED;
                        }

                        iRet = QS_API.qsFireKeyUpEvent(m_WebView, virtualKeyCode, flags, 0);
                        if (iRet != 0)
                        {
                            return IntPtr.Zero;
                        }

                        break;
                    }
                case (uint)WinConst.WM_CHAR:
                    {
                        uint charCode = (uint)wParam.ToInt32();
                        uint flags = 0;
                        if (((lParam.ToInt32() >> 16) & (int)WinConst.KF_REPEAT) != 0)
                        {
                            flags |= (uint)qsKeyFlags.QS_REPEAT;
                        }

                        if (((lParam.ToInt32() >> 16) & (int)WinConst.KF_EXTENDED) != 0)
                        {
                            flags |= (uint)qsKeyFlags.QS_EXTENDED;
                        }

                        iRet = QS_API.qsFireKeyPressEvent(m_WebView, charCode, flags, 0);
                        if (iRet != 0)
                        {
                            return IntPtr.Zero;
                        }

                        break;
                    }
                case (uint)WinConst.WM_LBUTTONDOWN:
                case (uint)WinConst.WM_MBUTTONDOWN:
                case (uint)WinConst.WM_RBUTTONDOWN:
                case (uint)WinConst.WM_LBUTTONDBLCLK:
                case (uint)WinConst.WM_MBUTTONDBLCLK:
                case (uint)WinConst.WM_RBUTTONDBLCLK:
                case (uint)WinConst.WM_LBUTTONUP:
                case (uint)WinConst.WM_MBUTTONUP:
                case (uint)WinConst.WM_RBUTTONUP:
                case (uint)WinConst.WM_MOUSEMOVE:
                    {
                        if (msg == (uint)WinConst.WM_LBUTTONDOWN || msg == (uint)WinConst.WM_MBUTTONDOWN || msg == (uint)WinConst.WM_RBUTTONDOWN)
                        {
                            if (QS_Common.GetFocus() != hWnd)
                            {
                                QS_Common.SetFocus(hWnd);
                            }

                            QS_Common.SetCapture(hWnd);
                        }
                        else if (msg == (uint)WinConst.WM_LBUTTONUP || msg == (uint)WinConst.WM_MBUTTONUP || msg == (uint)WinConst.WM_RBUTTONUP)
                        {
                            QS_Common.ReleaseCapture();
                        }

                        uint flags = 0;
                        int x = lParam.LOWORD();
                        int y = lParam.HIWORD();

                        if ((wParam.ToInt32() & (int)WinConst.MK_CONTROL) != 0)
                        {
                            flags |= (uint)qsMouseFlags.QS_CONTROL;
                        }

                        if ((wParam.ToInt32() & (int)WinConst.MK_SHIFT) != 0)
                        {
                            flags |= (uint)qsMouseFlags.QS_SHIFT;
                        }

                        if ((wParam.ToInt32() & (int)WinConst.MK_LBUTTON) != 0)
                        {
                            flags |= (uint)qsMouseFlags.QS_LBUTTON;
                        }

                        if ((wParam.ToInt32() & (int)WinConst.MK_MBUTTON) != 0)
                        {
                            flags |= (uint)qsMouseFlags.QS_MBUTTON;
                        }

                        if ((wParam.ToInt32() & (int)WinConst.MK_RBUTTON) != 0)
                        {
                            flags |= (uint)qsMouseFlags.QS_RBUTTON;
                        }

                        iRet = QS_API.qsFireMouseEvent(m_WebView, msg, x, y, flags);
                        if (iRet != 0)
                        {
                            return IntPtr.Zero;
                        }

                        break;
                    }
                case (uint)WinConst.WM_CONTEXTMENU:
                    {
                        POINT pt;
                        uint flags = 0;

                        pt.x = lParam.LOWORD();
                        pt.y = lParam.HIWORD();

                        if (pt.x != -1 && pt.y != -1)
                        {
                            QS_Common.ScreenToClient(hWnd, ref pt);
                        }

                        if ((wParam.ToInt32() & (int)WinConst.MK_CONTROL) != 0)
                        {
                            flags |= (uint)qsMouseFlags.QS_CONTROL;
                        }

                        if ((wParam.ToInt32() & (int)WinConst.MK_SHIFT) != 0)
                        {
                            flags |= (uint)qsMouseFlags.QS_SHIFT;
                        }

                        if ((wParam.ToInt32() & (int)WinConst.MK_LBUTTON) != 0)
                        {
                            flags |= (uint)qsMouseFlags.QS_LBUTTON;
                        }

                        if ((wParam.ToInt32() & (int)WinConst.MK_MBUTTON) != 0)
                        {
                            flags |= (uint)qsMouseFlags.QS_MBUTTON;
                        }

                        if ((wParam.ToInt32() & (int)WinConst.MK_RBUTTON) != 0)
                        {
                            flags |= (uint)qsMouseFlags.QS_RBUTTON;
                        }

                        iRet = QS_API.qsFireContextMenuEvent(m_WebView, pt.x, pt.y, flags);
                        if (iRet != 0)
                        {
                            return IntPtr.Zero;
                        }

                        break;
                    }
                case (uint)WinConst.WM_MOUSEWHEEL:
                    {
                        POINT pt;
                        uint flags = 0;
                        int delta = wParam.HIWORD();

                        pt.x = lParam.LOWORD();
                        pt.y = lParam.HIWORD();

                        QS_Common.ScreenToClient(hWnd, ref pt);

                        if ((wParam.ToInt32() & (int)WinConst.MK_CONTROL) != 0)
                        {
                            flags |= (uint)qsMouseFlags.QS_CONTROL;
                        }

                        if ((wParam.ToInt32() & (int)WinConst.MK_SHIFT) != 0)
                        {
                            flags |= (uint)qsMouseFlags.QS_SHIFT;
                        }

                        if ((wParam.ToInt32() & (int)WinConst.MK_LBUTTON) != 0)
                        {
                            flags |= (uint)qsMouseFlags.QS_LBUTTON;
                        }

                        if ((wParam.ToInt32() & (int)WinConst.MK_MBUTTON) != 0)
                        {
                            flags |= (uint)qsMouseFlags.QS_MBUTTON;
                        }

                        if ((wParam.ToInt32() & (int)WinConst.MK_RBUTTON) != 0)
                        {
                            flags |= (uint)qsMouseFlags.QS_RBUTTON;
                        }

                        iRet = QS_API.qsFireMouseWheelEvent(m_WebView, pt.x, pt.y, delta, flags);
                        if (iRet != 0)
                        {
                            return IntPtr.Zero;
                        }

                        break;
                    }
                case (uint)WinConst.WM_SETFOCUS:
                    {
                        QS_API.qsSetFocus(m_WebView);
                        return IntPtr.Zero;
                    }
                case (uint)WinConst.WM_KILLFOCUS:
                    {
                        QS_API.qsKillFocus(m_WebView);
                        return IntPtr.Zero;
                    }
                case (uint)WinConst.WM_SETCURSOR:
                    {
                        iRet = QS_API.qsFireWindowsMessage(m_WebView, hWnd, (uint)WinConst.WM_SETCURSOR, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero);
                        if (iRet != 0)
                        {
                            return IntPtr.Zero;
                        }

                        break;
                    }
                case (uint)WinConst.WM_IME_STARTCOMPOSITION:
                    {
                        iRet = QS_API.qsFireWindowsMessage(m_WebView, hWnd, (uint)WinConst.WM_IME_STARTCOMPOSITION, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero);
                        if (iRet != 0)
                        {
                            return IntPtr.Zero;
                        }

                        break;
                    }
                case (uint)WinConst.WM_INPUTLANGCHANGE:
                    {
                        return QS_Common.DefWindowProc(hWnd, msg, wParam, lParam);
                    }
                default:
                    {
                        break;
                    }
            }

            return QS_Common.CallWindowProc(m_WinProc, hWnd, msg, wParam, lParam);
        }

        #endregion

        #region --------------------------- 自定义事件，封装MB回调 ---------------------------

        public event EventHandler<PaintBitUpdatedEventArgs> onPaintBitUpdated
        {
            add
            {
                m_qsPaintBitUpdatedHandler += value;
            }

            remove
            {
                m_qsPaintBitUpdatedHandler -= value;
            }
        }

        public event EventHandler<BlinkThreadInitEventArgs> onBlinkThreadInit
        {
            add
            {
                m_qsBlinkThreadInitHandler += value;
            }

            remove
            {
                m_qsBlinkThreadInitHandler -= value;
            }
        }

        public event EventHandler<GetPdfPageDataEventArgs> onGetPdfPageData
        {
            add
            {
                m_qsGetPdfPageDataHandler += value;
            }

            remove
            {
                m_qsGetPdfPageDataHandler -= value;
            }
        }

        public event EventHandler<RunJsEventArgs> onRunJs
        {
            add
            {
                m_qsRunJsHandler += value;
            }

            remove
            {
                m_qsRunJsHandler -= value;
            }
        }

        public event EventHandler<JsQueryEventArgs> onJsQuery
        {
            add
            {
                m_qsJsQueryHandler += value;
            }

            remove
            {
                m_qsJsQueryHandler -= value;
            }
        }

        public event EventHandler<TitleChangedEventArgs> onTitleChanged
        {
            add
            {
                if (m_qsTitleChangedHandler == null)
                {
                    QS_API.qsOnTitleChanged(m_WebView, m_qsTitleChangedCallback, IntPtr.Zero);
                }

                m_qsTitleChangedHandler += value;
            }

            remove
            {
                m_qsTitleChangedHandler -= value;

                if (m_qsTitleChangedHandler == null)
                {
                    QS_API.qsOnTitleChanged(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<MouseOverUrlChangedEventArgs> onMouseOverUrlChanged
        {
            add
            {
                if (m_qsMouseOverUrlChangedHandler == null)
                {

                }

                m_qsMouseOverUrlChangedHandler += value;
            }

            remove
            {
                m_qsMouseOverUrlChangedHandler -= value;

                if (m_qsMouseOverUrlChangedHandler == null)
                {

                }
            }
        }

        public event EventHandler<UrlChangedEventArgs> onUrlChanged
        {
            add
            {
                if (m_qsUrlChangedHandler == null)
                {
                    QS_API.qsOnURLChanged(m_WebView, m_qsUrlChangedCallback, IntPtr.Zero);
                }

                m_qsUrlChangedHandler += value;
            }

            remove
            {
                m_qsUrlChangedHandler -= value;

                if (m_qsUrlChangedHandler == null)
                {
                    QS_API.qsOnURLChanged(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<UrlChangedEventArgs2> onUrlChanged2
        {
            add
            {
                if (m_qsUrlChangedHandler2 == null)
                {
                    QS_API.qsOnURLChanged2(m_WebView, m_qsUrlChangedCallback2, IntPtr.Zero);
                }

                m_qsUrlChangedHandler2 += value;
            }

            remove
            {
                m_qsUrlChangedHandler2 -= value;

                if (m_qsUrlChangedHandler2 == null)
                {
                    QS_API.qsOnURLChanged2(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<AlertBoxEventArgs> onAlertBox
        {
            add
            {
                if (m_qsAlertBoxHandler == null)
                {
                    QS_API.qsOnAlertBox(m_WebView, m_qsAlertBoxCallback, IntPtr.Zero);
                }

                m_qsAlertBoxHandler += value;
            }

            remove
            {
                m_qsAlertBoxHandler -= value;

                if (m_qsAlertBoxHandler == null)
                {
                    QS_API.qsOnAlertBox(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<ConfirmBoxEventArgs> onConfirmBox
        {
            add
            {
                if (m_qsConfirmBoxHandler == null)
                {
                    QS_API.qsOnConfirmBox(m_WebView, m_qsConfirmBoxCallback, IntPtr.Zero);
                }

                m_qsConfirmBoxHandler += value;
            }

            remove
            {
                m_qsConfirmBoxHandler -= value;

                if (m_qsConfirmBoxHandler == null)
                {
                    QS_API.qsOnConfirmBox(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<PromptBoxEventArgs> onPromptBox
        {
            add
            {
                if (m_qsPromptBoxHandler == null)
                {
                    QS_API.qsOnPromptBox(m_WebView, m_qsPromptBoxCallback, IntPtr.Zero);
                }

                m_qsPromptBoxHandler += value;
            }

            remove
            {
                m_qsPromptBoxHandler -= value;

                if (m_qsPromptBoxHandler == null)
                {
                    QS_API.qsOnPromptBox(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<NavigationEventArgs> onNavigation
        {
            add
            {
                if (m_qsNavigationHandler == null)
                {
                    QS_API.qsOnNavigation(m_WebView, m_qsNavigationCallback, IntPtr.Zero);
                }

                m_qsNavigationHandler += value;
            }

            remove
            {
                m_qsNavigationHandler -= value;
                if (m_qsNavigationHandler == null)
                {
                    QS_API.qsOnNavigation(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<NavigationEventArgs> OnNavigationSync
        {
            add
            {
                if (m_qsNavigationHandler == null)
                {
                    QS_API.qsOnNavigation(m_WebView, m_qsNavigationCallback, IntPtr.Zero);
                }

                m_qsNavigationHandler += value;
            }

            remove
            {
                m_qsNavigationHandler -= value;
                if (m_qsNavigationHandler == null)
                {
                    QS_API.qsOnNavigation(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<CreateViewEventArgs> onCreateView
        {
            add
            {
                if (m_qsCreateViewHandler == null)
                {
                    QS_API.qsOnCreateView(m_WebView, m_qsCreateViewCallback, IntPtr.Zero);
                }

                m_qsCreateViewHandler += value;
            }

            remove
            {
                m_qsCreateViewHandler -= value;

                if (m_qsCreateViewHandler == null)
                {
                    QS_API.qsOnCreateView(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<DocumentReadyEventArgs> onDocumentReady
        {
            add
            {
                if (m_qsDocumentReadyHandler == null)
                {
                    QS_API.qsOnDocumentReady(m_WebView, m_qsDocumentReadyCallback, IntPtr.Zero);
                }

                m_qsDocumentReadyHandler += value;
            }

            remove
            {
                m_qsDocumentReadyHandler -= value;

                if (m_qsDocumentReadyHandler == null)
                {
                    QS_API.qsOnDocumentReady(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<CloseEventArgs> onClose
        {
            add
            {
                if (m_qsCloseHandler == null)
                {
                    QS_API.qsOnClose(m_WebView, m_qsCloseCallback, IntPtr.Zero);
                }

                m_qsCloseHandler += value;
            }

            remove
            {
                m_qsCloseHandler -= value;

                if (m_qsCloseHandler == null)
                {
                    QS_API.qsOnClose(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<DestroyEventArgs> onDestroy
        {
            add
            {
                if (m_qsDestroyHandler == null)
                {
                    QS_API.qsOnDestroy(m_WebView, m_qsDestroyCallback, IntPtr.Zero);
                }

                m_qsDestroyHandler += value;
            }

            remove
            {
                m_qsDestroyHandler -= value;

                if (m_qsDestroyHandler == null)
                {
                    QS_API.qsOnDestroy(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<ShowDevtoolsEventArgs> onShowDevtools
        {
            add
            {
                m_qsShowDevtoolsHandler += value;
            }

            remove
            {
                m_qsShowDevtoolsHandler -= value;
            }
        }

        public event EventHandler<DidCreateScriptContextEventArgs> onDidCreateScriptContext
        {
            add
            {
                if (m_qsDidCreateScriptContextHandler == null)
                {
                    QS_API.qsOnDidCreateScriptContext(m_WebView, m_qsDidCreateScriptContextCallback, IntPtr.Zero);
                }

                m_qsDidCreateScriptContextHandler += value;
            }

            remove
            {
                m_qsDidCreateScriptContextHandler -= value;

                if (m_qsDidCreateScriptContextHandler == null)
                {
                    QS_API.qsOnDidCreateScriptContext(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<GetPluginListEventArgs> onGetPluginList
        {
            add
            {
                if (m_qsGetPluginListHandler == null)
                {
                    QS_API.qsOnPluginList(m_WebView, m_qsGetPluginListCallback, IntPtr.Zero);
                }

                m_qsGetPluginListHandler += value;
            }

            remove
            {
                m_qsGetPluginListHandler -= value;

                if (m_qsGetPluginListHandler == null)
                {
                    QS_API.qsOnPluginList(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<LoadingFinishEventArgs> onLoadingFinish
        {
            add
            {
                if (m_qsLoadingFinishHandler == null)
                {
                    QS_API.qsOnLoadingFinish(m_WebView, m_qsLoadingFinishCallback, IntPtr.Zero);
                }

                m_qsLoadingFinishHandler += value;
            }

            remove
            {
                m_qsLoadingFinishHandler -= value;

                if (m_qsLoadingFinishHandler == null)
                {
                    QS_API.qsOnLoadingFinish(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<DownloadEventArgs> onDownload
        {
            add
            {
                if (m_qsDownloadHandler == null)
                {
                    QS_API.qsOnDownload(m_WebView, m_qsDownloadCallback, IntPtr.Zero);
                }

                m_qsDownloadHandler += value;
            }

            remove
            {
                m_qsDownloadHandler -= value;

                if (m_qsDownloadHandler == null)
                {
                    QS_API.qsOnDownload(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<ConsoleEventArgs> onConsole
        {
            add
            {
                if (m_qsConsoleHandler == null)
                {
                    QS_API.qsOnConsole(m_WebView, m_qsConsoleCallback, IntPtr.Zero);
                }

                m_qsConsoleHandler += value;
            }

            remove
            {
                m_qsConsoleHandler -= value;

                if (m_qsConsoleHandler == null)
                {
                    QS_API.qsOnConsole(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<LoadUrlBeginEventArgs> onLoadUrlBegin
        {
            add
            {
                if (m_qsLoadUrlBeginHandler == null)
                {
                    QS_API.qsOnLoadUrlBegin(m_WebView, m_qsLoadUrlBeginCallback, IntPtr.Zero);
                }

                m_qsLoadUrlBeginHandler += value;
            }

            remove
            {
                m_qsLoadUrlBeginHandler -= value;

                if (m_qsLoadUrlBeginHandler == null)
                {
                    QS_API.qsOnLoadUrlBegin(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<LoadUrlEndEventArgs> onLoadUrlEnd
        {
            add
            {
                if (m_qsLoadUrlEndHandler == null)
                {
                    QS_API.qsOnLoadUrlEnd(m_WebView, m_qsLoadUrlEndCallback, IntPtr.Zero);
                }

                m_qsLoadUrlEndHandler += value;
            }

            remove
            {
                m_qsLoadUrlEndHandler -= value;

                if (m_qsLoadUrlEndHandler == null)
                {
                    QS_API.qsOnLoadUrlEnd(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<LoadUrlFailEventArgs> onLoadUrlFail
        {
            add
            {
                if (m_qsLoadUrlFailHandler == null)
                {
                    QS_API.qsOnLoadUrlFail(m_WebView, m_qsLoadUrlFailCallback, IntPtr.Zero);
                }

                m_qsLoadUrlFailHandler += value;
            }

            remove
            {
                m_qsLoadUrlFailHandler -= value;

                if (m_qsLoadUrlFailHandler == null)
                {
                    QS_API.qsOnLoadUrlFail(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<WillReleaseScriptContextEventArgs> onWillReleaseScriptContext
        {
            add
            {
                m_qsWillReleaseScriptContextHandler += value;
            }

            remove
            {
                m_qsWillReleaseScriptContextHandler -= value;
            }
        }

        public event EventHandler<NetResponseEventArgs> onNetResponse
        {
            add
            {
                m_qsNetResponseHandler += value;
            }

            remove
            {
                m_qsNetResponseHandler -= value;
            }
        }

        public event EventHandler<NetGetFaviconEventArgs> onNetGetFavicon
        {
            add
            {
                m_qsNetGetFaviconHandler += value;
            }

            remove
            {
                m_qsNetGetFaviconHandler -= value;
            }
        }

        public event EventHandler<CanGoBackForwardEventArgs> onCanGoBackForward
        {
            add
            {
                m_qsCanGoBackForwardHandler += value;
            }

            remove
            {
                m_qsCanGoBackForwardHandler -= value;
            }
        }

        public event EventHandler<GetCookieEventArgs> onGetCookie
        {
            add
            {
                m_qsGetCookieHandler += value;
            }

            remove
            {
                m_qsGetCookieHandler -= value;
            }
        }

        public event EventHandler<GetSourceEventArgs> onGetSource
        {
            add
            {
                m_qsGetSourceHandler += value;
            }

            remove
            {
                m_qsGetSourceHandler -= value;
            }
        }

        public event EventHandler<GetContentAsMarkupEventArgs> onGetContentAsMarkup
        {
            add
            {
                m_qsGetContentAsMarkupHandler += value;
            }

            remove
            {
                m_qsGetContentAsMarkupHandler -= value;
            }
        }

        public event EventHandler<UrlRequestWillRedirectEventArgs> onUrlRequestWillRedirect
        {
            add
            {
                m_qsUrlRequestWillRedirectHandler += value;
            }

            remove
            {
                m_qsUrlRequestWillRedirectHandler -= value;
            }
        }

        public event EventHandler<UrlRequestDidReceiveResponseEventArgs> onUrlRequestDidReceiveResponse
        {
            add
            {
                m_qsUrlRequestDidReceiveResponseHandler += value;
            }

            remove
            {
                m_qsUrlRequestDidReceiveResponseHandler -= value;
            }
        }

        public event EventHandler<UrlRequestDidReceiveDataEventArgs> onUrlRequestDidReceiveData
        {
            add
            {
                m_qsUrlRequestDidReceiveDataHandler += value;
            }

            remove
            {
                m_qsUrlRequestDidReceiveDataHandler -= value;
            }
        }

        public event EventHandler<UrlRequestDidFailEventArgs> onUrlRequestDidFail
        {
            add
            {
                m_qsUrlRequestDidFailHandler += value;
            }

            remove
            {
                m_qsUrlRequestDidFailHandler -= value;
            }
        }

        public event EventHandler<UrlRequestDidFinishLoadingEventArgs> onUrlRequestDidFinishLoading
        {
            add
            {
                m_qsUrlRequestDidFinishLoadingHandler += value;
            }

            remove
            {
                m_qsUrlRequestDidFinishLoadingHandler -= value;
            }
        }

        public event EventHandler<NetJobDataRecvEventArgs> onNetJobDataRecv
        {
            add
            {
                m_qsNetJobDataRecvHandler += value;
            }

            remove
            {
                m_qsNetJobDataRecvHandler -= value;
            }
        }

        public event EventHandler<NetJobDataFinishEventArgs> onNetJobDataFinish
        {
            add
            {
                m_qsNetJobDataFinishHandler += value;
            }

            remove
            {
                m_qsNetJobDataFinishHandler -= value;
            }
        }

        public event EventHandler<PopupDialogSaveNameEventArgs> onPopupDialogSaveName
        {
            add
            {
                m_qsPopupDialogSaveNameHandler += value;
            }

            remove
            {
                m_qsPopupDialogSaveNameHandler -= value;
            }
        }

        public event EventHandler<DownloadInBlinkThreadEventArgs> onDownloadInBlinkThread
        {
            add
            {
                if (m_qsDownloadInBlinkThreadHandler == null)
                {
                    QS_API.qsOnDownloadInBlinkThread(m_WebView, m_qsDownloadInBlinkThreadCallback, IntPtr.Zero);
                }

                m_qsDownloadInBlinkThreadHandler += value;
            }

            remove
            {
                m_qsDownloadInBlinkThreadHandler -= value;

                if (m_qsDownloadInBlinkThreadHandler == null)
                {
                    QS_API.qsOnDownloadInBlinkThread(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<PrintPdfDataEventArgs> onPrintPdfData
        {
            add
            {
                m_qsPrintPdfDataHandler += value;
            }

            remove
            {
                m_qsPrintPdfDataHandler -= value;
            }
        }

        public event EventHandler<PrintBitmapEventArgs> onPrintBitmap
        {
            add
            {
                m_qsPrintBitmapHandler += value;
            }

            remove
            {
                m_qsPrintBitmapHandler -= value;
            }
        }

        public event EventHandler<WindowClosingEventArgs> onWindowClosing
        {
            add
            {
                m_qsWindowClosingHandler += value;
            }

            remove
            {
                m_qsWindowClosingHandler -= value;
            }
        }

        public event EventHandler<WindowDestroyEventArgs> onWindowDestroy
        {
            add
            {
                m_qsWindowDestroyHandler += value;
            }

            remove
            {
                m_qsWindowDestroyHandler -= value;
            }
        }

        public event EventHandler<DraggableRegionsChangedEventArgs> onDraggableRegionsChanged
        {
            add
            {
                m_qsDraggableRegionsChangedHandler += value;
            }

            remove
            {
                m_qsDraggableRegionsChangedHandler -= value;
            }
        }

        public event EventHandler<PrintingEventArgs> onPrinting
        {
            add
            {
                if (m_qsPrintingHandler == null)
                {
                    QS_API.qsOnPrinting(m_WebView, m_qsPrintingCallback, IntPtr.Zero);
                }

                m_qsPrintingHandler += value;
            }

            remove
            {
                m_qsPrintingHandler -= value;

                if (m_qsPrintingHandler == null)
                {
                    QS_API.qsOnPrinting(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<ImageBufferToDataUrlEventArgs> onImageBufferToDataURL
        {
            add
            {
                if (m_qsImageBufferToDataUrlHandler == null)
                {
                    QS_API.qsOnImageBufferToDataURL(m_WebView, m_qsImageBufferToDataUrlCallback, IntPtr.Zero);
                }

                m_qsImageBufferToDataUrlHandler += value;
            }

            remove
            {
                m_qsImageBufferToDataUrlHandler -= value;

                if (m_qsImageBufferToDataUrlHandler == null)
                {
                    QS_API.qsOnImageBufferToDataURL(m_WebView, null, IntPtr.Zero);
                }
            }
        }

        public event EventHandler<ScreenshotEventArgs> onScreenshot
        {
            add
            {
                m_qsScreenshotHandler += value;
            }

            remove
            {
                m_qsScreenshotHandler -= value;
            }
        }

        public event EventHandler<CallUiThreadEventArgs> onCallUiThread
        {
            add
            {
                m_qsCallUiThreadHandler += value;
            }

            remove
            {
                m_qsCallUiThreadHandler -= value;
            }
        }

        #endregion

        #region --------------------------- 设置回调事件，给事件参数赋值 ---------------------------

        protected void SetCallBack()
        {
            m_qsPaintUpdatedCallback = new qsPaintUpdatedCallback(onPaintUpdated);

            m_qsWndProcCallback = new WndProcCallback(onWndProc);

            m_qsPaintBitUpdatedCallback = new qsPaintBitUpdatedCallback((IntPtr webView, IntPtr param, IntPtr buffer, IntPtr rect, int width, int height) =>
            {
                m_qsPaintBitUpdatedHandler?.Invoke(this, new PaintBitUpdatedEventArgs(webView, param, buffer, rect, width, height));
            });

            m_qsBlinkThreadInitCallback = new qsOnBlinkThreadInitCallback((IntPtr param) =>
            {
                m_qsBlinkThreadInitHandler?.Invoke(this, new BlinkThreadInitEventArgs(param));
            });

            m_qsGetPdfPageDataCallback = new qsOnGetPdfPageDataCallback((IntPtr webView, IntPtr param, IntPtr data, uint size) =>
            {
                m_qsGetPdfPageDataHandler?.Invoke(this, new GetPdfPageDataEventArgs(webView, param, data, size));
            });

            m_qsRunJsCallback = new qsRunJsCallback((IntPtr webView, IntPtr param, IntPtr es, ulong v) =>
            {
                m_qsRunJsHandler?.Invoke(this, new RunJsEventArgs(webView, param, es, v));
            });

            m_qsJsQueryCallback = new qsJsQueryCallback((IntPtr webView, IntPtr param, IntPtr es, ulong queryId, int customMsg, IntPtr request) =>
            {
                m_qsJsQueryHandler?.Invoke(this, new JsQueryEventArgs(webView, param, es, queryId, customMsg, request));
            });

            m_qsTitleChangedCallback = new qsTitleChangedCallback((IntPtr WebView, IntPtr param, IntPtr title) =>
            {
                m_qsTitleChangedHandler?.Invoke(this, new TitleChangedEventArgs(WebView, param, title));
            });

            m_qsMouseOverUrlChangedCallback = new qsMouseOverUrlChangedCallback((IntPtr webView, IntPtr param, IntPtr url) =>
            {
                m_qsMouseOverUrlChangedHandler?.Invoke(this, new MouseOverUrlChangedEventArgs(webView, param, url));
            });

            m_qsUrlChangedCallback = new qsUrlChangedCallback((IntPtr webView, IntPtr param, IntPtr url, int canGoBack, int canGoForward) =>
            {
                m_qsUrlChangedHandler?.Invoke(this, new UrlChangedEventArgs(webView, param, url, canGoBack, canGoForward));
            });

            m_qsUrlChangedCallback2 = new qsUrlChangedCallback2((IntPtr webView, IntPtr param, IntPtr frameId, IntPtr url) =>
            {
                m_qsUrlChangedHandler2?.Invoke(this, new UrlChangedEventArgs2(webView, param, frameId, url));
            });

            m_qsAlertBoxCallback = new qsAlertBoxCallback((IntPtr webView, IntPtr param, IntPtr msg) =>
            {
                m_qsAlertBoxHandler?.Invoke(this, new AlertBoxEventArgs(webView, param, msg));
            });

            m_qsConfirmBoxCallback = new qsConfirmBoxCallback((IntPtr webView, IntPtr param, IntPtr msg) =>
            {
                int iRet = 0;
                if (m_qsConfirmBoxHandler != null)
                {
                    ConfirmBoxEventArgs e = new ConfirmBoxEventArgs(webView, param, msg);
                    m_qsConfirmBoxHandler(this, e);

                    iRet = e.iRet;
                }

                return iRet;
            });

            m_qsPromptBoxCallback = new qsPromptBoxCallback((IntPtr webView, IntPtr param, IntPtr msg, IntPtr defaultResult) =>
            {
                IntPtr ptrRet = IntPtr.Zero;
                if (m_qsPromptBoxHandler != null)
                {
                    PromptBoxEventArgs e = new PromptBoxEventArgs(webView, param, msg, defaultResult);
                    m_qsPromptBoxHandler(this, e);

                    ptrRet = e.ptrRet;
                }

                return ptrRet;
            });

            m_qsNavigationCallback = new qsNavigationCallback((IntPtr webView, IntPtr param, qsNavigationType navigationType, IntPtr url) =>
            {
                int iRet = 0;
                if (m_qsNavigationHandler != null)
                {
                    NavigationEventArgs e = new NavigationEventArgs(webView, param, navigationType, url);
                    m_qsNavigationHandler(this, e);

                    iRet = e.iRet;
                }

                return iRet;
            });

            m_qsCreateViewCallback = new qsCreateViewCallback((IntPtr webView, IntPtr param, qsNavigationType navigationType, IntPtr url, IntPtr windowFeatures) =>
            {
                IntPtr ptrRet = IntPtr.Zero;
                if (m_qsCreateViewHandler != null)
                {
                    CreateViewEventArgs e = new CreateViewEventArgs(webView, param, navigationType, url, windowFeatures);
                    m_qsCreateViewHandler(this, e);

                    ptrRet = e.NewWebViewHandle;
                }

                return ptrRet;
            });

            m_qsDocumentReadyCallback = new qsDocumentReadyCallback((IntPtr webView, IntPtr param, IntPtr frame) =>
            {
                m_qsDocumentReadyHandler?.Invoke(this, new DocumentReadyEventArgs(webView, param, frame));
            });

            m_qsCloseCallback = new qsCloseCallback((IntPtr webView, IntPtr param, IntPtr unuse) =>
            {
                int iRet = 1;
                if (m_qsCloseHandler != null)
                {
                    CloseEventArgs e = new CloseEventArgs(webView, param, unuse);
                    m_qsCloseHandler(this, e);

                    iRet = e.iCancel;
                }

                return iRet;
            });

            m_qsDestroyCallback = new qsDestroyCallback((IntPtr webView, IntPtr param, IntPtr unuse) =>
            {
                int iRet = 1;
                if (m_qsDestroyHandler != null)
                {
                    DestroyEventArgs e = new DestroyEventArgs(webView, param, unuse);
                    m_qsDestroyHandler(this, e);

                    iRet = e.iRet;
                }

                return iRet;
            });

            m_qsShowDevtoolsCallback = new qsOnShowDevtoolsCallback((IntPtr webView, IntPtr param) =>
            {
                m_qsShowDevtoolsHandler?.Invoke(this, new ShowDevtoolsEventArgs(webView, param));
            });

            m_qsDidCreateScriptContextCallback = new qsDidCreateScriptContextCallback((IntPtr webView, IntPtr param, IntPtr frameId, IntPtr context, int extensionGroup, int worldId) =>
            {
                m_qsDidCreateScriptContextHandler?.Invoke(this, new DidCreateScriptContextEventArgs(webView, param, frameId, context, extensionGroup, worldId));
            });

            m_qsGetPluginListCallback = new qsGetPluginListCallback((int refresh, IntPtr pluginListBuilder, IntPtr param) =>
            {
                int iRet = 0;
                if (m_qsGetPluginListHandler != null)
                {
                    GetPluginListEventArgs e = new GetPluginListEventArgs(refresh, pluginListBuilder, param);
                    m_qsGetPluginListHandler(this, e);

                    iRet = e.iRefresh;
                }

                return iRet;
            });

            m_qsLoadingFinishCallback = new qsLoadingFinishCallback((IntPtr webView, IntPtr param, IntPtr frameId, IntPtr url, qsLoadingResult result, IntPtr failedReason) =>
            {
                m_qsLoadingFinishHandler?.Invoke(this, new LoadingFinishEventArgs(webView, param, frameId, url, result, failedReason));
            });

            m_qsConsoleCallback = new qsConsoleCallback((IntPtr webView, IntPtr param, qsConsoleLevel level, IntPtr message, IntPtr sourceName, uint sourceLine, IntPtr stackTrace) =>
            {
                m_qsConsoleHandler?.Invoke(this, new ConsoleEventArgs(webView, param, level, message, sourceName, sourceLine, stackTrace));
            });

            m_qsLoadUrlBeginCallback = new qsLoadUrlBeginCallback((IntPtr webView, IntPtr param, IntPtr url, IntPtr job) =>
            {
                int iRet = 0;
                if (m_qsLoadUrlBeginHandler != null)
                {
                    LoadUrlBeginEventArgs e = new LoadUrlBeginEventArgs(webView, param, url, job);
                    m_qsLoadUrlBeginHandler(this, e);

                    iRet = e.bCancel ? 1 : 0;
                }

                return iRet;
            });

            m_qsLoadUrlEndCallback = new qsLoadUrlEndCallback((IntPtr webView, IntPtr param, IntPtr url, IntPtr job, IntPtr buf, int len) =>
            {
                m_qsLoadUrlEndHandler?.Invoke(this, new LoadUrlEndEventArgs(webView, param, url, job, buf, len));
            });

            m_qsLoadUrlFailCallback = new qsLoadUrlFailCallback((IntPtr webView, IntPtr param, IntPtr url, IntPtr job) =>
            {
                m_qsLoadUrlFailHandler?.Invoke(this, new LoadUrlFailEventArgs(webView, param, url, job));
            });

            m_qsWillReleaseScriptContextCallback = new qsWillReleaseScriptContextCallback((IntPtr webView, IntPtr param, IntPtr frameId, IntPtr context, int worldId) =>
            {
                m_qsWillReleaseScriptContextHandler?.Invoke(this, new WillReleaseScriptContextEventArgs(webView, param, frameId, context, worldId));
            });

            m_qsNetResponseCallback = new qsNetResponseCallback((IntPtr webView, IntPtr param, IntPtr url, IntPtr job) =>
            {
                int iRet = 0;
                if (m_qsNetResponseHandler != null)
                {
                    NetResponseEventArgs e = new NetResponseEventArgs(webView, param, url, job);
                    m_qsNetResponseHandler(this, e);

                    iRet = e.iRet;
                }

                return iRet;
            });

            m_qsNetGetFaviconCallback = new qsNetGetFaviconCallback((IntPtr webView, IntPtr param, IntPtr url, IntPtr buf) =>
            {
                m_qsNetGetFaviconHandler?.Invoke(this, new NetGetFaviconEventArgs(webView, param, url, buf));
            });

            m_qsCanGoBackForwardCallback = new qsCanGoBackForwardCallback((IntPtr webView, IntPtr param, qsAsynRequestState state, int b) =>
            {
                m_qsCanGoBackForwardHandler?.Invoke(this, new CanGoBackForwardEventArgs(webView, param, state, b));
            });

            m_qsGetCookieCallback = new qsGetCookieCallback((IntPtr webView, IntPtr param, qsAsynRequestState state, IntPtr cookie) =>
            {
                m_qsGetCookieHandler?.Invoke(this, new GetCookieEventArgs(webView, param, state, cookie));
            });

            m_qsGetSourceCallback = new qsGetSourceCallback((IntPtr webView, IntPtr param, IntPtr mhtml) =>
            {
                m_qsGetSourceHandler?.Invoke(this, new GetSourceEventArgs(webView, param, mhtml));
            });

            m_qsGetContentAsMarkupCallback = new qsGetContentAsMarkupCallback((IntPtr webView, IntPtr param, IntPtr content, uint size) =>
            {
                m_qsGetContentAsMarkupHandler?.Invoke(this, new GetContentAsMarkupEventArgs(webView, param, content, size));
            });

            m_qsUrlRequestWillRedirectCallback = new qsOnUrlRequestWillRedirectCallback((IntPtr webView, IntPtr param, IntPtr oldRequest, IntPtr request, IntPtr redirectResponse) =>
            {
                m_qsUrlRequestWillRedirectHandler?.Invoke(this, new UrlRequestWillRedirectEventArgs(webView, param, oldRequest, request, redirectResponse));
            });

            m_qsUrlRequestDidReceiveResponseCallback = new qsOnUrlRequestDidReceiveResponseCallback((IntPtr webView, IntPtr param, IntPtr request, IntPtr response) =>
            {
                m_qsUrlRequestDidReceiveResponseHandler?.Invoke(this, new UrlRequestDidReceiveResponseEventArgs(webView, param, request, response));
            });

            m_qsUrlRequestDidReceiveDataCallback = new qsOnUrlRequestDidReceiveDataCallback((IntPtr webView, IntPtr param, IntPtr request, IntPtr data, int dataLength) =>
            {
                m_qsUrlRequestDidReceiveDataHandler?.Invoke(this, new UrlRequestDidReceiveDataEventArgs(webView, param, request, data, dataLength));
            });

            m_qsUrlRequestDidFailCallback = new qsOnUrlRequestDidFailCallback((IntPtr webView, IntPtr param, IntPtr request, IntPtr error) =>
            {
                m_qsUrlRequestDidFailHandler?.Invoke(this, new UrlRequestDidFailEventArgs(webView, param, request, error));
            });

            m_qsUrlRequestDidFinishLoadingCallback = new qsOnUrlRequestDidFinishLoadingCallback((IntPtr webView, IntPtr param, IntPtr request, double finishTime) =>
            {
                m_qsUrlRequestDidFinishLoadingHandler?.Invoke(this, new UrlRequestDidFinishLoadingEventArgs(webView, param, request, finishTime));
            });

            m_qsDownloadCallback = new qsDownloadCallback((IntPtr webView, IntPtr param, IntPtr frameId, IntPtr url, IntPtr downloadJob) =>
            {
                int iRet = 1;
                if (m_qsDownloadHandler != null)
                {
                    DownloadEventArgs e = new DownloadEventArgs(webView, param, frameId, url, downloadJob);
                    m_qsDownloadHandler(this, e);

                    iRet = e.Cancel ? 0 : 1;
                }

                return iRet;
            });

            m_qsNetJobDataRecvCallback = new qsNetJobDataRecvCallback((IntPtr ptr, IntPtr job, IntPtr data, int length) =>
            {
                m_qsNetJobDataRecvHandler?.Invoke(this, new NetJobDataRecvEventArgs(ptr, job, data, length));
            });

            m_qsNetJobDataFinishCallback = new qsNetJobDataFinishCallback((IntPtr ptr, IntPtr job, qsLoadingResult result) =>
            {
                m_qsNetJobDataFinishHandler?.Invoke(this, new NetJobDataFinishEventArgs(ptr, job, result));
            });

            m_qsPopupDialogSaveNameCallback = new qsPopupDialogSaveNameCallback((IntPtr ptr, IntPtr filePath) =>
            {
                m_qsPopupDialogSaveNameHandler?.Invoke(this, new PopupDialogSaveNameEventArgs(ptr, filePath));
            });

            m_qsDownloadInBlinkThreadCallback = new qsDownloadInBlinkThreadCallback((IntPtr webView, IntPtr param, uint expectedContentLength, IntPtr url, IntPtr mime, IntPtr disposition, IntPtr job, IntPtr dataBind) =>
            {
                qsDownloadOpt downloadRet = qsDownloadOpt.QS_DownloadOptCacheData;
                if (m_qsDownloadInBlinkThreadHandler != null)
                {
                    DownloadInBlinkThreadEventArgs e = new DownloadInBlinkThreadEventArgs(webView, param, expectedContentLength, url, mime, disposition, job, dataBind);
                    m_qsDownloadInBlinkThreadHandler(this, e);

                    downloadRet = e.DownloadRet;
                }

                return downloadRet;
            });

            m_qsPrintPdfDataCallback = new qsPrintPdfDataCallback((IntPtr webView, IntPtr param, IntPtr datas) =>
            {
                m_qsPrintPdfDataHandler?.Invoke(this, new PrintPdfDataEventArgs(webView, param, datas));
            });

            m_qsPrintBitmapCallback = new qsPrintBitmapCallback((IntPtr webView, IntPtr param, IntPtr data, uint size) =>
            {
                m_qsPrintBitmapHandler?.Invoke(this, new PrintBitmapEventArgs(webView, param, data, size));
            });

            m_qsWindowClosingCallback = new qsWindowClosingCallback((IntPtr webView, IntPtr param) =>
            {
                int iRet = 0;
                if (m_qsWindowClosingHandler != null)
                {
                    WindowClosingEventArgs e = new WindowClosingEventArgs(webView, param);
                    m_qsWindowClosingHandler(this, e);

                    iRet = e.iCancel;
                }

                return iRet;
            });

            m_qsWindowDestroyCallback = new qsWindowDestroyCallback((IntPtr webView, IntPtr param) =>
            {
                m_qsWindowDestroyHandler?.Invoke(this, new WindowDestroyEventArgs(webView, param));
            });

            m_qsDraggableRegionsChangedCallback = new qsDraggableRegionsChangedCallback((IntPtr webView, IntPtr param, IntPtr rects, int rectCount) =>
            {
                m_qsDraggableRegionsChangedHandler?.Invoke(this, new DraggableRegionsChangedEventArgs(webView, param, rects, rectCount));
            });

            m_qsPrintingCallback = new qsPrintingCallback((IntPtr webView, IntPtr param, qsPrintintStep step, IntPtr hDC, IntPtr settings, int pageCount) =>
            {
                int iRet = 0;
                if (m_qsPrintingHandler != null)
                {
                    PrintingEventArgs e = new PrintingEventArgs(webView, param, step, hDC, settings, pageCount);
                    m_qsPrintingHandler(this, e);

                    iRet = e.iPageCount;
                }

                return iRet;
            });

            m_qsImageBufferToDataUrlCallback = new qsImageBufferToDataURLCallback((IntPtr webView, IntPtr param, IntPtr data, uint size) =>
            {
                m_qsImageBufferToDataUrlHandler?.Invoke(this, new ImageBufferToDataUrlEventArgs(webView, param, data, size));
                return QS_API.qsCreateString(data, size);
            });

            m_qsScreenshotCallback = new qsOnScreenshotCallback((IntPtr webView, IntPtr param, IntPtr data, uint size) =>
            {
                m_qsScreenshotHandler?.Invoke(this, new ScreenshotEventArgs(webView, param, data, size));
            });

            m_qsCallUiThreadCallback = new qsOnCallUiThreadCallback((IntPtr webView, IntPtr paramOnInThread) =>
            {
                m_qsCallUiThreadHandler?.Invoke(this, new CallUiThreadEventArgs(webView, paramOnInThread));
            });
        }

        #endregion

        #region --------------------------- 各种属性封装 ---------------------------

        /// <summary>
        /// WebView
        /// </summary>
        private IntPtr m_WebView { get; set; }

        /// <summary>
        /// 获取 WebView 句柄
        /// </summary>
        public IntPtr Handle
        {
            get { return m_WebView; }
        }

        /// <summary>
        /// 获取或设置 WebView 的名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 获取网页标题
        /// </summary>
        public string Title
        {
            get { return QS_API.qsGetTitle(m_WebView).UTF8PtrToStr(); }
        }

        /// <summary>
        /// 获取网页URL
        /// </summary>
        public string URL
        {
            get { return QS_API.qsGetUrl(m_WebView).UTF8PtrToStr(); }
        }

        /// <summary>
        /// 是否允许鼠标
        /// </summary>
        public bool MouseEnabled
        {
            set { QS_API.qsSetMouseEnabled(m_WebView, value ? 1 : 0); }
        }

        /// <summary>
        /// 是否允许触屏
        /// </summary>
        public bool TouchEnabled
        {
            set { QS_API.qsSetTouchEnabled(m_WebView, value ? 1 : 0); }
        }

        /// <summary>
        /// 是否允许菜单
        /// </summary>
        public bool ContextMenuEnabled
        {
            set { QS_API.qsSetContextMenuEnabled(m_WebView, value ? 1 : 0); }
        }

        /// <summary>
        /// 设置是否导航到新窗口
        /// </summary>
        public bool NavigationToNewWindowEnable
        {
            set { QS_API.qsSetNavigationToNewWindowEnable(m_WebView, value ? 1 : 0); }
        }

        /// <summary>
        /// 是否允许无头模式，启用后可关闭渲染
        /// </summary>
        public bool HeadlessEnabled
        {
            set { QS_API.qsSetHeadlessEnabled(m_WebView, value ? 1 : 0); }
        }

        /// <summary>
        /// 是否允许拖放
        /// </summary>
        public bool DragDropEnable
        {
            set { QS_API.qsSetDragDropEnable(m_WebView, value ? 1 : 0); }
        }

        /// <summary>
        /// 是否允许拖动
        /// </summary>
        public bool DragEnable
        {
            set { QS_API.qsSetDragEnable(m_WebView, value ? 1 : 0); }
        }

        /// <summary>
        /// 设置页面代理，全局生效
        /// </summary>
        public qsProxy Proxy
        {
            set { QS_API.qsSetProxy(m_WebView, ref value); }
        }

        /// <summary>
        /// 设置UA
        /// </summary>
        public string UserAgent
        {
            set { QS_API.qsSetUserAgent(m_WebView, value.StrToUtf8Ptr()); }
        }

        /// <summary>
        /// 是否允许Cookie
        /// </summary>
        public bool CookieEnabled
        {
            set { QS_API.qsSetCookieEnabled(m_WebView, value ? 1 : 0); }
        }

        /// <summary>
        /// 设置Cookie文件的完整路径，注意：是完整的，路径，不存在会自动新建
        /// </summary>
        public string CookieFullPath
        {
            set { QS_API.qsSetCookieJarFullPath(m_WebView, value); }
        }

        /// <summary>
        /// 设置Storage文件的完整路径，注意：是完整的，路径，不存在会自动新建
        /// </summary>
        public string StorageFullPath
        {
            set { QS_API.qsSetLocalStorageFullPath(m_WebView, value); }
        }

        /// <summary>
        /// 是否允许跨域检查
        /// </summary>
        public bool CspCheckEnable
        {
            set { QS_API.qsSetCspCheckEnable(m_WebView, value ? 1 : 0); }
        }

        /// <summary>
        /// 是否允许Npapi
        /// </summary>
        public bool NpapiEnabled
        {
            set { QS_API.qsSetNpapiPluginsEnabled(m_WebView, value ? 1 : 0); }
        }

        /// <summary>
        /// 是否允许内存缓存
        /// </summary>
        public bool MemoryCacheEnable
        {
            set { QS_API.qsSetMemoryCacheEnable(m_WebView, value ? 1 : 0); }
        }

        /// <summary>
        /// 设置或获取缩放比例，默认是1（100%）
        /// </summary>
        public float ZoomFactor
        {
            set { QS_API.qsSetZoomFactor(m_WebView, value); }
            get { return QS_API.qsGetZoomFactor(m_WebView); }
        }

        /// <summary>
        /// 是否允许磁盘缓存
        /// </summary>
        public bool DiskCacheEnabled
        {
            set { QS_API.qsSetDiskCacheEnabled(m_WebView, value ? 1 : 0); }
        }

        /// <summary>
        /// 设置磁盘缓存路径
        /// </summary>
        public string DiskCachePath
        {
            set { QS_API.qsSetDiskCachePath(m_WebView, value.StrToUnicodePtr()); }
        }

        /// <summary>
        /// 设置磁盘缓存大小
        /// </summary>
        public uint DiskCacheLimit
        {
            set { QS_API.qsSetDiskCacheLimit(m_WebView, value); }
        }

        /// <summary>
        /// 设置磁盘缓存限制
        /// </summary>
        public uint DiskCacheLimitDisk
        {
            set { QS_API.qsSetDiskCacheLimitDisk(m_WebView, value); }
        }

        /// <summary>
        /// 设置磁盘缓存级别
        /// </summary>
        public int DiskCacheLevel
        {
            set { QS_API.qsSetDiskCacheLevel(m_WebView, value); }
        }

        /// <summary>
        /// 获取主句柄
        /// </summary>
        public IntPtr GetHostHWND
        {
            get { return QS_API.qsGetHostHWND(m_WebView); }
        }

        /// <summary>
        /// 是否允许nodejs
        /// </summary>
        public bool SetNodeJsEnable
        {
            set { QS_API.qsSetNodeJsEnable(m_WebView, value ? 1 : 0); }
        }

        /// <summary>
        /// 解锁ViewDC
        /// </summary>
        public bool UnlockViewDC
        {
            set { QS_API.qsUnlockViewDC(m_WebView); }
        }

        /// <summary>
        /// 强制唤醒MB，好像没啥用
        /// </summary>
        public bool Wake
        {
            set { QS_API.qsWake(m_WebView); }
        }

        /// <summary>
        /// 设置支持高度DPI
        /// </summary>
        public bool EnableHighDPISupport
        {
            set { QS_API.qsEnableHighDPISupport(); }
        }

        /// <summary>
        /// 离屏模式下控制是否自动上屏
        /// </summary>
        public bool SetAutoDrawToHwnd
        {
            set { QS_API.qsSetAutoDrawToHwnd(m_WebView, value ? 1 : 0); }
        }

        /// <summary>
        /// 获取页面主框架的ID
        /// </summary>
        public IntPtr GetMainFrame
        {
            get { return QS_API.qsWebFrameGetMainFrame(m_WebView); }
        }

        #endregion

        #region --------------------------- 各种方法封装 ---------------------------

        private IntPtr m_hWnd;
        private IntPtr m_WinProc;
        private qsSettings m_settings;

        private Timer RefreshTimer = new Timer();
        private WndProcCallback m_procRefresh = null;
        private qsPaintUpdatedCallback m_updatedRefresh = null;

        /// <summary>
        /// 构造函数，初始化
        /// </summary>
        public QS_WebView()
        {
            m_settings.mask = qsSettingMask.QS_ENABLE_NODEJS;
            QS_API.qsInit(ref m_settings);

            SetCallBack();

            m_WebView = QS_API.qsCreateWebView();
            QS_API.qsSetHandle(m_WebView, m_hWnd);

            RefreshTimer.Interval = 60 * 1000;
            RefreshTimer.Elapsed += RefreshTimer_Elapsed;
            RefreshTimer.Start();
        }

        // GC蛋疼的回收机制，委托传给非托管对象后，GC没法监控其使用情况而自动将其释放，定时引用下，防止被释放而引发的崩溃
        private void RefreshTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            m_procRefresh = m_qsWndProcCallback;
            m_updatedRefresh = m_qsPaintUpdatedCallback;
        }

        /// <summary>
        /// 设置无窗口模式下的绘制偏移。在某些情况下（主要是离屏模式），绘制的地方不在真窗口的(0, 0)处，就需要手动调用此接口
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void SetHandleOffset(int x, int y)
        {
            QS_API.qsSetHandleOffset(m_WebView, x, y);
        }

        /// <summary>
        /// 销毁WebView
        /// </summary>
        public void Dispose()
        {
            RefreshTimer.Stop();
            QS_Common.SetWindowLong(m_hWnd, (int)WinConst.GWL_WNDPROC, null);
            QS_API.qsOnPaintUpdated(m_WebView, null, m_hWnd);
            QS_API.qsSetHandle(m_WebView, IntPtr.Zero);
            QS_API.qsDestroyWebView(m_WebView);
            QS_API.qsUninit();

            m_WinProc = IntPtr.Zero;
            m_WebView = IntPtr.Zero;
            m_hWnd = IntPtr.Zero;
        }

        /// <summary>
        /// 将m_WebView和某个窗体控件绑定，通常是pannel
        /// </summary>
        /// <param name="hWnd">控件句柄</param>
        /// <returns></returns>
        public bool Bind(IntPtr hWnd)
        {
            if (m_hWnd == hWnd)
            {
                return true;
            }
            else
            {
                m_hWnd = hWnd;
            }

            if (m_WebView == IntPtr.Zero)
            {
                m_WebView = QS_API.qsCreateWebView();
                if (m_WebView == IntPtr.Zero)
                {
                    return false;
                }
            }

            QS_API.qsSetHandle(m_WebView, m_hWnd);
            QS_API.qsOnPaintUpdated(m_WebView, m_qsPaintUpdatedCallback, m_hWnd);
            m_WinProc = QS_Common.SetWindowLong(m_hWnd, (int)WinConst.GWL_WNDPROC, m_qsWndProcCallback);

            RECT rc = new RECT();
            QS_Common.GetClientRect(m_hWnd, ref rc);
            QS_API.qsResize(m_WebView, rc.Right - rc.Left, rc.Bottom - rc.Top);

            return true;
        }

        /// <summary>
        /// 设置居中（相对父控件）
        /// </summary>
        public void SetCenter()
        {
            QS_API.qsMoveToCenter(m_WebView);
        }

        /// <summary>
        /// 打开网页
        /// </summary>
        /// <param name="strUrl"></param>
        public void LoadUrl(string strUrl)
        {
            QS_API.qsLoadURL(m_WebView, strUrl.StrToUtf8Ptr());
        }

        public void qsLoadHtmlWithBaseUrl(string strHtml, string strUrl)
        {
            IntPtr ptrHtml = strHtml.StrToUtf8Ptr();
            IntPtr ptrUrl = strUrl.StrToUtf8Ptr();

            QS_API.qsLoadHtmlWithBaseUrl(m_WebView, ptrHtml, ptrUrl);
        }

        /// <summary>
        /// Post数据
        /// </summary>
        /// <param name="strUrl"></param>
        /// <param name="data"></param>
        /// <param name="iDataLen"></param>
        public void PostData(string strUrl, byte[] data, int iDataLen)
        {
            QS_API.qsPostURL(m_WebView, strUrl.StrToUtf8Ptr(), data, iDataLen);
        }

        /// <summary>
        /// 设置硬件参数，可以用于模拟手机环境等
        /// </summary>
        /// <param name="strDevice">设备的字符串。可取值有：
        /// "navigator.maxTouchPoints"此时 paramInt 需要被设置，表示 touch 的点数。
        /// "navigator.platform"此时 paramStr 需要被设置，表示js里获取的 navigator.platform字符串。
        /// "navigator.hardwareConcurrency"此时 paramInt 需要被设置，表示js里获取的 navigator.hardwareConcurrency 整数值。
        /// "screen.width"此时 paramInt 需要被设置，表示js里获取的 screen.width 整数值。
        /// "screen.height"此时 paramInt 需要被设置，表示js里获取的 screen.height 整数值。
        /// "screen.availWidth"此时 paramInt 需要被设置，表示js里获取的 screen.availWidth 整数值。
        /// "screen.availHeight"此时 paramInt 需要被设置，表示js里获取的 screen.availHeight 整数值。
        /// "screen.pixelDepth"此时 paramInt 需要被设置，表示js里获取的 screen.pixelDepth 整数值。
        /// "screen.pixelDepth"目前等价于"screen.pixelDepth"。
        /// "window.devicePixelRatio"同上</param>
        /// <param name="strParam"></param>
        /// <param name="iParam"></param>
        /// <param name="fParam"></param>
        public void SetDeviceParameter(string strDevice, string strParam, int iParam = 0, float fParam = 0)
        {
            IntPtr ptrDevice = strDevice.StrToUtf8Ptr();
            IntPtr ptrParam = strParam.StrToUtf8Ptr();

            QS_API.qsSetDeviceParameter(m_WebView, ptrDevice, ptrParam, iParam, fParam);
        }

        /// <summary>
        /// 开启一些实验性选项。
        /// </summary>
        /// <param name="strDebug">
        /// "showDevTools"	开启开发者工具，此时param要填写开发者工具的资源路径，如file:///c:/miniblink-release/front_end/inspector.html。注意param此时必须是utf8编
        /// "wakeMinInterval" 设置帧率，默认值是10，值越大帧率越低
        /// "drawMinInterval" 设置帧率，默认值是3，值越大帧率越低
        /// "antiAlias" 设置抗锯齿渲染。param必须设置为"1"
        /// "minimumFontSize" 最小字体
        /// "minimumLogicalFontSize" 最小逻辑字体
        /// "defaultFontSize" 默认字体
        /// "defaultFixedFontSize" 默认fixed字体</param>
        /// <param name="strParam"></param>
        public void SetDebugConfig(string strDebug, string strParam)
        {
            QS_API.qsSetDebugConfig(m_WebView, strDebug, strParam);
        }

        /// <summary>
        /// 重新载入
        /// </summary>
        public void Reload()
        {
            QS_API.qsReload(m_WebView);
        }

        /// <summary>
        /// 停止载入
        /// </summary>
        public void StopLoading()
        {
            QS_API.qsStopLoading(m_WebView);
        }

        /// <summary>
        /// 后退
        /// </summary>
        public void GoBack()
        {
            QS_API.qsGoBack(m_WebView);
        }

        /// <summary>
        /// 前进
        /// </summary>
        public void GoForward()
        {
            QS_API.qsGoForward(m_WebView);
        }

        /// <summary>
        /// 执行js，结构在m_qsRunJsCallback中获取
        /// </summary>
        /// <param name="strJs"></param>
        public void RunJs(string strJs, string strParam)
        {
            IntPtr ptrJs = strJs.StrToUtf8Ptr();
            IntPtr ptrParam = strParam.StrToUtf8Ptr();
            IntPtr ptrFrameId = QS_API.qsWebFrameGetMainFrame(m_WebView);

            QS_API.qsRunJs(m_WebView, ptrFrameId, ptrJs, 1, m_qsRunJsCallback, ptrParam, IntPtr.Zero);
        }

        /// <summary>
        /// 异步运行js（暂不可用）
        /// </summary>
        /// <param name="ptrFrameId"></param>
        /// <param name="strJs"></param>
        /// <returns></returns>
        public ulong RunJsSync(IntPtr ptrFrameId, string strJs)
        {
            IntPtr ptrJs = strJs.StrToUtf8Ptr();

            return QS_API.qsRunJsSync(m_WebView, ptrFrameId, ptrJs, 1);
        }

        /// <summary>
        /// 注册js通知native的回调。用于js调用c#函数
        /// </summary>
        /// <param name="strParam"></param>
        public void qsOnJsQuery(string strParam)
        {
            IntPtr ptrParam = strParam.StrToUtf8Ptr();
            QS_API.qsOnJsQuery(m_WebView, m_qsJsQueryCallback, ptrParam);
        }

        /// <summary>
        /// 返回c#函数的值给js
        /// </summary>
        /// <param name="iQueryId"></param>
        /// <param name="iCustomMsg"></param>
        /// <param name="strResponse"></param>
        public void ResponseQuery(ulong iQueryId, int iCustomMsg, string strResponse)
        {
            IntPtr ptrResponse = strResponse.StrToUtf8Ptr();
            QS_API.qsResponseQuery(m_WebView, iQueryId, iCustomMsg, ptrResponse);
        }

        /// <summary>
        /// 判断是否是主框架
        /// </summary>
        /// <param name="ptrFrameId"></param>
        /// <returns></returns>
        public bool IsMainFrame(IntPtr ptrFrameId)
        {
            int iRet = QS_API.qsIsMainFrame(m_WebView, ptrFrameId);
            return iRet == 1 ? true : false;
        }

        /// <summary>
        /// 获取页面html代码
        /// </summary>
        public void GetSource()
        {
            QS_API.qsGetSource(m_WebView, m_qsGetSourceCallback, IntPtr.Zero);
        }

        /// <summary>
        /// 设置网络数据，需在OnLoadUrlBegin事件中调用
        /// </summary>
        /// <param name="ptrJob"></param>
        /// <param name="data"></param>
        public void NetSetData(IntPtr ptrJob, byte[] data)
        {
            QS_API.qsNetSetData(ptrJob, data, data.Length);
        }

        /// <summary>
        /// 设置网络数据，需在OnLoadUrlBegin事件中调用
        /// </summary>
        /// <param name="ptrJob"></param>
        /// <param name="strData">string数据</param>
        public void NetSetData(IntPtr ptrJob, string strData)
        {
            byte[] data = Encoding.UTF8.GetBytes(strData);
            NetSetData(ptrJob, data);
        }

        /// <summary>
        /// 设置网络数据，需在OnLoadUrlBegin事件中调用
        /// </summary>
        /// <param name="ptrJob"></param>
        /// <param name="img">图片数据</param>
        /// <param name="fmt">图片格式</param>
        public void NetSetData(IntPtr ptrJob, Image img, ImageFormat fmt)
        {
            byte[] data = null;
            using (MemoryStream ms = new MemoryStream())
            {
                img.Save(ms, fmt);
                data = ms.GetBuffer();
            }

            NetSetData(ptrJob, data);
        }

        /// <summary>
        /// 需在OnLoadUrlBegin事件中调用。如果设置了此钩子，则会缓存获取到的网络数据，
        /// 并在这次网络请求结束后调用qsOnLoadUrlEnd设置的回调，同时传递缓存的数据。在此期间，qs不会处理网络数据。
        /// 如果在OnLoadUrlBegin事件里没设置qsNetHookRequest，则不会触发qsOnLoadUrlEnd回调。
        /// </summary>
        /// <param name="ptrJob"></param>
        public void HookRequest(IntPtr ptrJob)
        {
            QS_API.qsNetHookRequest(ptrJob);
        }

        /// <summary>
        /// 需在OnLoadUrlBegin事件中调用，修改请求的url
        /// </summary>
        /// <param name="ptrJob"></param>
        /// <param name="strUrl"></param>
        public void ChangeRequestUrl(IntPtr ptrJob, string strUrl)
        {
            QS_API.qsNetChangeRequestUrl(ptrJob, strUrl);
        }

        /// <summary>
        /// 需在OnLoadUrlBegin事件中调用，继续被中断的任务
        /// </summary>
        /// <param name="ptrJob"></param>
        public void ContinueJob(IntPtr ptrJob)
        {
            QS_API.qsNetContinueJob(ptrJob);
        }

        /// <summary>
        /// 需在OnLoadUrlBegin事件中调用，获取渲染线程原始http头
        /// </summary>
        /// <param name="ptrJob"></param>
        /// <returns></returns>
        public qsSlist GetRawHttpHeadInBlinkThread(IntPtr ptrJob)
        {
            IntPtr ptrStruct = QS_API.qsNetGetRawHttpHeadInBlinkThread(ptrJob);
            return (qsSlist)ptrStruct.UTF8PtrToStruct(typeof(qsSlist));
        }

        /// <summary>
        /// 需在OnLoadUrlBegin事件中调用，获取渲染线程原始响应头
        /// </summary>
        /// <param name="ptrJob"></param>
        /// <returns></returns>
        public qsSlist GetRawResponseHeadInBlinkThread(IntPtr ptrJob)
        {
            IntPtr ptrStruct = QS_API.qsNetGetRawResponseHeadInBlinkThread(ptrJob);
            return (qsSlist)ptrStruct.UTF8PtrToStruct(typeof(qsSlist));
        }

        /// <summary>
        /// 需在OnLoadUrlBegin事件中调用，挂起任务，异步提交，配合ContinueJob使用
        /// </summary>
        /// <param name="ptrJob"></param>
        public void HoldJobToAsynCommit(IntPtr ptrJob)
        {
            QS_API.qsNetHoldJobToAsynCommit(ptrJob);
        }

        /// <summary>
        /// 需在OnLoadUrlBegin事件中调用，取消请求
        /// </summary>
        /// <param name="ptrJob"></param>
        public void CancelRequest(IntPtr ptrJob)
        {
            QS_API.qsNetCancelRequest(ptrJob);
        }

        /// <summary>
        /// 设置web通信管道，会将结果设置到qsWebsocketHookCallbacks结构体
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="ptrParam"></param>
        public void SetWebsocketCallback(ref qsWebsocketHookCallbacks callback, IntPtr ptrParam)
        {
            QS_API.qsNetSetWebsocketCallback(m_WebView, ref callback, ptrParam);
        }

        /// <summary>
        /// 通过web通信管道发送文本数据
        /// </summary>
        /// <param name="strChannel"></param>
        /// <param name="data"></param>
        /// <param name="iDataLen"></param>
        public void SendWsText(string strChannel, byte[] data, uint iDataLen)
        {
            QS_API.qsNetSendWsText(strChannel.StrToUtf8Ptr(), data, iDataLen);
        }

        /// <summary>
        /// 通过web通信管道发送二进制数据
        /// </summary>
        /// <param name="strChannel"></param>
        /// <param name="data"></param>
        /// <param name="iDataLen"></param>
        public void SendWsBlob(string strChannel, byte[] data, uint iDataLen)
        {
            QS_API.qsNetSendWsBlob(strChannel.StrToUtf8Ptr(), data, iDataLen);
        }

        /// <summary>
        /// 获取Post数据内容
        /// </summary>
        /// <param name="ptrJob"></param>
        /// <returns></returns>
        public qsPostBodyElement[] GetPostBody(IntPtr ptrJob)
        {
            IntPtr ptrBody = QS_API.qsNetGetPostBody(ptrJob);
            qsPostBodyElements elements = (qsPostBodyElements)ptrBody.UTF8PtrToStruct(typeof(qsPostBodyElements));

            qsPostBodyElement[] HttpDataArr = new qsPostBodyElement[elements.elementSize];
            IntPtr ptrElement = elements.element;

            for (int i = 0; i < elements.elementSize; i++)
            {
                var temp = (IntPtr)ptrElement.UTF8PtrToStruct(typeof(IntPtr));
                HttpDataArr[i] = (qsPostBodyElement)temp.UTF8PtrToStruct(typeof(qsPostBodyElement));
                ptrElement = new IntPtr(ptrElement.ToInt64() + IntPtr.Size);
            }

            return HttpDataArr;
        }

        /// <summary>
        /// 释放Http数据
        /// </summary>
        /// <param name="elements"></param>
        public void FreePostBodyElements(qsPostBodyElements elements)
        {
            IntPtr ptrStruct = elements.StructToUTF8Ptr();
            QS_API.qsNetFreePostBodyElements(ptrStruct);
        }

        /// <summary>
        /// 创建Http数据
        /// </summary>
        /// <param name="iLength"></param>
        /// <returns></returns>
        public qsPostBodyElements CreatePostBodyElements(uint length)
        {
            IntPtr ptrStruct = QS_API.qsNetCreatePostBodyElements(m_WebView, length);

            return (qsPostBodyElements)ptrStruct.UTF8PtrToStruct(typeof(qsPostBodyElements));
        }

        /// <summary>
        /// 创建Http数据
        /// </summary>
        /// <param name="iLength"></param>
        /// <returns></returns>
        public qsPostBodyElement CreatePostBodyElement()
        {
            IntPtr ptrStruct = QS_API.qsNetCreatePostBodyElement(m_WebView);

            return (qsPostBodyElement)ptrStruct.UTF8PtrToStruct(typeof(qsPostBodyElement));
        }

        /// <summary>
        /// 释放Http数据
        /// </summary>
        /// <param name="elements"></param>
        public void FreePostBodyElement(qsPostBodyElements element)
        {
            IntPtr ptrStruct = element.StructToUTF8Ptr();
            QS_API.qsNetFreePostBodyElement(ptrStruct);
        }

        /// <summary>
        /// 创建网络请求
        /// </summary>
        /// <param name="strUrl"></param>
        /// <param name="strMethod"></param>
        /// <param name="strMime"></param>
        /// <returns>返回qsWebUrlRequestPtr类型指针，暂未定义</returns>
        public IntPtr CreateWebUrlRequest(string strUrl, string strMethod, string strMime)
        {
            IntPtr ptrUrl = strUrl.StrToUtf8Ptr();
            IntPtr ptrMethod = strMethod.StrToUtf8Ptr();
            IntPtr ptrMime = strMime.StrToUtf8Ptr();

            return QS_API.qsNetCreateWebUrlRequest(ptrUrl, ptrMethod, ptrMime);
        }

        /// <summary>
        /// 向请求中添加http头字段
        /// </summary>
        /// <param name="ptrRequest">qsWebUrlRequestPtr类型指针，暂未定义</param>
        /// <param name="strName"></param>
        /// <param name="strValue"></param>
        public void AddHTTPHeaderFieldToUrlRequest(IntPtr ptrRequest, string strName, string strValue)
        {
            IntPtr ptrName = strName.StrToUtf8Ptr();
            IntPtr ptrValue = strValue.StrToUtf8Ptr();

            QS_API.qsNetAddHTTPHeaderFieldToUrlRequest(ptrRequest, ptrName, ptrValue);
        }

        /// <summary>
        /// 开始请求
        /// </summary>
        /// <param name="ptrRequest">qsWebUrlRequestPtr类型指针，暂未定义</param>
        /// <param name="RequestCallbacks"></param>
        /// <returns></returns>
        public int StartUrlRequest(IntPtr ptrRequest, qsUrlRequestCallbacks RequestCallbacks)
        {
            return QS_API.qsNetStartUrlRequest(m_WebView, ptrRequest, IntPtr.Zero, ref RequestCallbacks);
        }

        /// <summary>
        /// 获取响应码
        /// </summary>
        /// <param name="ptrResponse">qsWebUrlResponsePtr类型指针，暂未定义</param>
        /// <returns></returns>
        public int GetHttpStatusCode(IntPtr ptrResponse)
        {
            return QS_API.qsNetGetHttpStatusCode(ptrResponse);
        }

        /// <summary>
        /// 获取请求方法
        /// </summary>
        /// <param name="ptrJob"></param>
        /// <returns></returns>
        public qsRequestType GetRequestMethod(IntPtr ptrJob)
        {
            return QS_API.qsNetGetRequestMethod(ptrJob);
        }

        /// <summary>
        /// 获取Content长度
        /// </summary>
        /// <param name="ptrResponse">qsWebUrlResponsePtr类型指针，暂未定义</param>
        /// <returns></returns>
        public long GetExpectedContentLength(IntPtr ptrResponse)
        {
            return QS_API.qsNetGetExpectedContentLength(ptrResponse);
        }

        /// <summary>
        /// 获取响应url
        /// </summary>
        /// <param name="ptrResponse">qsWebUrlResponsePtr类型指针，暂未定义</param>
        /// <returns></returns>
        public string GetResponseUrl(IntPtr ptrResponse)
        {
            IntPtr ptrUrl = QS_API.qsNetGetResponseUrl(ptrResponse);
            return ptrUrl.UTF8PtrToStr();
        }

        /// <summary>
        /// 取消网络请求
        /// </summary>
        /// <param name="iRequestId"></param>
        public void CancelWebUrlRequest(int iRequestId)
        {
            QS_API.qsNetCancelWebUrlRequest(iRequestId);
        }

        /// <summary>
        /// 设置mimeType，需在OnLoadUrlBegin事件中调用
        /// </summary>
        /// <param name="ptrJob"></param>
        /// <param name="strMimeType"></param>
        public void SetMimeType(IntPtr ptrJob, string strMimeType)
        {
            QS_API.qsNetSetMIMEType(ptrJob, strMimeType.StrToUtf8Ptr());
        }

        /// <summary>
        /// 获取mimeType，需在OnLoadUrlBegin事件中调用
        /// </summary>
        /// <param name="ptrJob"></param>
        /// <returns></returns>
        public string GetMimeType(IntPtr ptrJob)
        {
            IntPtr ptrRet = QS_API.qsNetGetMIMEType(ptrJob);
            return ptrRet.UTF8PtrToStr();
        }

        /// <summary>
        /// 设置HTTP请求头，需在OnLoadUrlBegin事件中调用
        /// </summary>
        /// <param name="ptrJob"></param>
        /// <param name="strKey"></param>
        /// <param name="strValue"></param>
        public void SetHttpHeaderField(IntPtr ptrJob, string strKey, string strValue, bool response = false)
        {
            IntPtr ptrKey = strKey.StrToUnicodePtr();
            IntPtr ptrValue = strValue.StrToUnicodePtr();
            QS_API.qsNetSetHTTPHeaderField(ptrJob, ptrKey, ptrValue, response ? 1 : 0);
        }

        /// <summary>
        /// 获取HTTP请求头字段，需在OnLoadUrlBegin事件中调用
        /// </summary>
        /// <param name="ptrJob"></param>
        /// <param name="strKey"></param>
        /// <param name="iRequestOrResponse">0为请求，1为响应</param>
        /// <returns></returns>
        public string GetHttpHeaderField(IntPtr ptrJob, string strKey, int iRequestOrResponse)
        {
            IntPtr ptrValue = QS_API.qsNetGetHTTPHeaderField(ptrJob, strKey, iRequestOrResponse);
            return ptrValue.UTF8PtrToStr();
        }

        /// <summary>
        /// 设置Cookie，格式必须是PRODUCTINFO=webxpress; domain=.fidelity.com; path=/; secure的标准格式
        /// </summary>
        /// <param name="strUrl"></param>
        /// <param name="strCookie"></param>
        public void SetCookie(string strUrl, string strCookie)
        {
            IntPtr ptrUrl = strUrl.StrToUtf8Ptr();
            IntPtr ptrCookie = strCookie.StrToUtf8Ptr();
            QS_API.qsSetCookie(m_WebView, ptrUrl, ptrCookie);
        }

        /// <summary>
        /// 设置菜单是否显示
        /// </summary>
        /// <param name="item"></param>
        /// <param name="bShow"></param>
        public void SetContextMenuItemShow(qsMenuItemId item, bool bShow)
        {
            QS_API.qsSetContextMenuItemShow(m_WebView, item, bShow ? 1 : 0);
        }

        /// <summary>
        /// 创建新窗口
        /// </summary>
        /// <param name="type"></param>
        /// <param name="hParent">父窗口句柄</param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="iWidth"></param>
        /// <param name="iHeight"></param>
        /// <returns>返回qsWebView指针</returns>
        public IntPtr CreateWebWindow(qsWindowType type, IntPtr hParent, int x, int y, int iWidth, int iHeight)
        {
            return QS_API.qsCreateWebWindow(type, hParent, x, y, iWidth, iHeight);
        }

        /// <summary>
        /// 创建客户端窗口
        /// </summary>
        /// <param name="hParent">父窗口句柄</param>
        /// <param name="iStyle"></param>
        /// <param name="iStyleEx"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="iWidth"></param>
        /// <param name="iHeight"></param>
        /// <returns>返回qsWebView指针</returns>
        public IntPtr CreateWebCustomWindow(IntPtr hParent, ulong iStyle, ulong iStyleEx, int x, int y, int iWidth, int iHeight)
        {
            return QS_API.qsCreateWebCustomWindow(hParent, iStyle, iStyleEx, x, y, iWidth, iHeight);
        }

        /// <summary>
        /// 创建MB内部使用的字符串
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string CreateString(string str)
        {
            IntPtr ptr = str.StrToUtf8Ptr();
            IntPtr ptrRet = QS_API.qsCreateString(ptr, (uint)str.Length);

            return ptrRet.UTF8PtrToStr();
        }

        /// <summary>
        /// 创建一段不以\0结尾的字符串，其实就是一段内存数据
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string CreateMemory(string str)
        {
            IntPtr ptr = str.StrToUtf8Ptr();
            IntPtr ptrRet = QS_API.qsCreateStringWithoutNullTermination(ptr, (uint)str.Length);

            return ptrRet.UTF8PtrToStr();
        }

        /// <summary>
        /// 删除CreateString或CreateMemory创建的字符串
        /// </summary>
        /// <param name="str"></param>
        public void DeleteString(string str)
        {
            QS_API.qsDeleteString(str.StrToUtf8Ptr());
        }

        /// <summary>
        /// 获取CreateString或CreateMemory创建的字符串长度
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public long GetStringLen(string str)
        {
            return QS_API.qsGetStringLen(str.StrToUtf8Ptr());
        }

        /// <summary>
        /// 获取CreateString或CreateMemory创建的字符串
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string GetString(string str)
        {
            IntPtr ptrRet = QS_API.qsGetString(str.StrToUtf8Ptr());
            return ptrRet.UTF8PtrToStr();
        }

        /// <summary>
        /// 添加插件目录
        /// </summary>
        /// <param name="strPah"></param>
        public void AddPluginDirectory(string strPah)
        {
            QS_API.qsAddPluginDirectory(m_WebView, strPah);
        }

        /// <summary>
        /// 设置资源自动清理时间间隔，默认是盟主说忘了
        /// </summary>
        /// <param name="iSecond"></param>
        public void SetResourceGc(int iSecond)
        {
            QS_API.qsSetResourceGc(m_WebView, iSecond);
        }

        /// <summary>
        /// Base64编码
        /// </summary>
        /// <param name="strUrl"></param>
        /// <returns></returns>
        public string Base64Encode(string strUrl)
        {
            IntPtr ptrUrl = strUrl.StrToUtf8Ptr();
            ptrUrl = QS_API.qsUtilBase64Encode(ptrUrl);

            return ptrUrl.UTF8PtrToStr();
        }

        /// <summary>
        /// Base64解码
        /// </summary>
        /// <param name="strUrl"></param>
        /// <returns></returns>
        public string Base64Decode(string strUrl)
        {
            IntPtr ptrUrl = strUrl.StrToUtf8Ptr();
            ptrUrl = QS_API.qsUtilBase64Decode(ptrUrl);

            return ptrUrl.UTF8PtrToStr();
        }

        /// <summary>
        /// url转义
        /// </summary>
        /// <param name="strUrl"></param>
        /// <returns></returns>
        public string EncodeURLEscape(string strUrl)
        {
            IntPtr ptrUrl = strUrl.StrToUtf8Ptr();
            ptrUrl = QS_API.qsUtilEncodeURLEscape(ptrUrl);

            return ptrUrl.UTF8PtrToStr();
        }

        /// <summary>
        /// url反转义
        /// </summary>
        /// <param name="strUrl"></param>
        /// <returns></returns>
        public string DecodeURLEscape(string strUrl)
        {
            IntPtr ptrUrl = strUrl.StrToUtf8Ptr();
            ptrUrl = QS_API.qsUtilDecodeURLEscape(ptrUrl);

            return ptrUrl.UTF8PtrToStr();
        }

        /// <summary>
        /// 创建V8引擎内存快照
        /// </summary>
        /// <param name="strUrl"></param>
        /// <returns></returns>
        public qsMemBuf CreateV8Snapshot(string strUrl)
        {
            IntPtr ptrUrl = strUrl.StrToUtf8Ptr();
            IntPtr ptrMem = QS_API.qsUtilCreateV8Snapshot(ptrUrl);

            return (qsMemBuf)ptrMem.UTF8PtrToStruct(typeof(qsMemBuf));
        }

        /// <summary>
        /// 创建内存缓存
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public qsMemBuf CreateMemBuf(byte[] data)
        {
            IntPtr ptrBuf = data.ByteToUtf8Ptr();
            IntPtr ptrMem = QS_API.qsCreateMemBuf(m_WebView, ptrBuf, (uint)data.Length);

            return (qsMemBuf)ptrMem.UTF8PtrToStruct(typeof(qsMemBuf));
        }

        /// <summary>
        /// 释放CreateMemBuf或CreateV8Snapshot创建的内存
        /// </summary>
        /// <param name="buf"></param>
        public void FreeMemBuf(qsMemBuf buf)
        {
            QS_API.qsFreeMemBuf(ref buf);
        }

        /// <summary>
        /// 查询能否前进
        /// </summary>
        /// <param name="strParam"></param>
        public void CanForward(string strParam)
        {
            IntPtr ptrParam = strParam.StrToUtf8Ptr();
            QS_API.qsCanGoForward(m_WebView, m_qsCanGoBackForwardCallback, ptrParam);
        }

        /// <summary>
        /// 查询能否后退
        /// </summary>
        /// <param name="strParam"></param>
        public void CanBack(string strParam)
        {
            IntPtr ptrParam = strParam.StrToUtf8Ptr();
            QS_API.qsCanGoBack(m_WebView, m_qsCanGoBackForwardCallback, ptrParam);
        }

        /// <summary>
        /// 获取cookie
        /// </summary>
        /// <param name="strParam"></param>
        public void GetCookie(string strParam)
        {
            IntPtr ptrParam = strParam.StrToUtf8Ptr();
            QS_API.qsGetCookie(m_WebView, m_qsGetCookieCallback, ptrParam);
        }

        /// <summary>
        /// 获取渲染线程的cookie
        /// </summary>
        /// <returns></returns>
        public string GetCookieOnBlinkThread()
        {
            IntPtr ptrRet = QS_API.qsGetCookieOnBlinkThread(m_WebView);
            return ptrRet.UTF8PtrToStr();
        }

        /// <summary>
        /// 清除cookie
        /// </summary>
        /// <returns></returns>
        public void ClearCookie()
        {
            QS_API.qsClearCookie(m_WebView);
        }

        /// <summary>
        /// 重新设置界面大小
        /// </summary>
        /// <param name="iWidth"></param>
        /// <param name="iHeight"></param>
        public void Resize(int iWidth, int iHeight)
        {
            QS_API.qsResize(m_WebView, iWidth, iHeight);
        }

        /// <summary>
        /// 执行cookie相关命令
        /// </summary>
        /// <param name="com"></param>
        public void qsPerformCookieCommand(qsCookieCommand com)
        {
            QS_API.qsPerformCookieCommand(m_WebView, com);
        }

        /// <summary>
        /// 全选
        /// </summary>
        public void EditorSelectAll()
        {
            QS_API.qsEditorSelectAll(m_WebView);
        }

        /// <summary>
        /// 复制
        /// </summary>
        public void EditorCopy()
        {
            QS_API.qsEditorCopy(m_WebView);
        }

        /// <summary>
        /// 剪切
        /// </summary>
        public void EditorCut()
        {
            QS_API.qsEditorCut(m_WebView);
        }

        /// <summary>
        /// 粘贴
        /// </summary>
        public void EditorPaste()
        {
            QS_API.qsEditorPaste(m_WebView);
        }

        /// <summary>
        /// 删除
        /// </summary>
        public void EditorDelete()
        {
            QS_API.qsEditorDelete(m_WebView);
        }

        /// <summary>
        /// 撤销
        /// </summary>
        public void EditorUndo()
        {
            QS_API.qsEditorUndo(m_WebView);
        }

        /// <summary>
        /// 发送鼠标消息
        /// </summary>
        /// <param name="iMessage"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="iFlags"></param>
        /// <returns></returns>
        public bool FireMouseEvent(uint iMessage, int x, int y, uint iFlags)
        {
            int iRet = QS_API.qsFireMouseEvent(m_WebView, iMessage, x, y, iFlags);
            return iRet == 1 ? true : false;
        }

        /// <summary>
        /// 发送菜单消息
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="iFlags"></param>
        /// <returns></returns>
        public bool FireContextMenuEvent(int x, int y, uint iFlags)
        {
            int iRet = QS_API.qsFireContextMenuEvent(m_WebView, x, y, iFlags);
            return iRet == 1 ? true : false;
        }

        /// <summary>
        /// 发送滚轮消息
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="iDelta"></param>
        /// <param name="iFlags"></param>
        /// <returns></returns>
        public bool FireMouseWheelEvent(int x, int y, int iDelta, uint iFlags)
        {
            int iRet = QS_API.qsFireMouseWheelEvent(m_WebView, x, y, iDelta, iFlags);
            return iRet == 1 ? true : false;
        }

        /// <summary>
        /// 发送按键按下消息
        /// </summary>
        /// <param name="iVirtualKeyCode"></param>
        /// <param name="iFlags"></param>
        /// <param name="iSystemKey"></param>
        /// <returns></returns>
        public bool FireKeyDownEvent(uint iVirtualKeyCode, uint iFlags, int iSystemKey)
        {
            int iRet = QS_API.qsFireKeyDownEvent(m_WebView, iVirtualKeyCode, iFlags, iSystemKey);
            return iRet == 1 ? true : false;
        }

        /// <summary>
        /// 发送按键抬起消息
        /// </summary>
        /// <param name="iVirtualKeyCode"></param>
        /// <param name="iFlags"></param>
        /// <param name="iSystemKey"></param>
        /// <returns></returns>
        public bool FireKeyUpEvent(uint iVirtualKeyCode, uint iFlags, int iSystemKey)
        {
            int iRet = QS_API.qsFireKeyUpEvent(m_WebView, iVirtualKeyCode, iFlags, iSystemKey);
            return iRet == 1 ? true : false;
        }

        /// <summary>
        /// 发送按键消息
        /// </summary>
        /// <param name="iCharCode"></param>
        /// <param name="iFlags"></param>
        /// <param name="iSystemKey"></param>
        /// <returns></returns>
        public bool FireKeyPressEvent(uint iCharCode, uint iFlags, int iSystemKey)
        {
            int iRet = QS_API.qsFireKeyPressEvent(m_WebView, iCharCode, iFlags, iSystemKey);
            return iRet == 1 ? true : false;
        }

        /// <summary>
        /// 发送通用消息
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="iMessage"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <param name="strResult"></param>
        /// <returns></returns>
        public bool FireWindowsMessage(IntPtr hWnd, uint iMessage, IntPtr wParam, IntPtr lParam)
        {
            int iRet = QS_API.qsFireWindowsMessage(m_WebView, hWnd, iMessage, wParam, lParam, IntPtr.Zero);
            return iRet == 1 ? true : false;
        }

        /// <summary>
        /// 设置焦点
        /// </summary>
        public void SetFocus()
        {
            QS_API.qsSetFocus(m_WebView);
        }

        /// <summary>
        /// 放弃焦点
        /// </summary>
        public void KillFocus()
        {
            QS_API.qsKillFocus(m_WebView);
        }

        /// <summary>
        /// 是否显示窗口
        /// </summary>
        public bool ShowWindow
        {
            set { QS_API.qsShowWindow(m_WebView, value ? 1 : 0); }
        }

        /// <summary>
        /// 插件相关，需在qsGetPluginListCallback中调用
        /// </summary>
        /// <param name="ptrBuilder"></param>
        /// <param name="strName"></param>
        /// <param name="strDesc"></param>
        /// <param name="strFile"></param>
        public void PluginListBuilderAddPlugin(IntPtr ptrBuilder, string strName, string strDesc, string strFile)
        {
            IntPtr ptrName = strName.StrToUtf8Ptr();
            IntPtr PtrDesc = strDesc.StrToUtf8Ptr();
            IntPtr PtrFile = strFile.StrToUtf8Ptr();

            QS_API.qsPluginListBuilderAddPlugin(ptrBuilder, ptrName, PtrDesc, PtrFile);
        }

        /// <summary>
        /// 插件相关，需在qsGetPluginListCallback中调用
        /// </summary>
        /// <param name="ptrBuilder"></param>
        /// <param name="strName"></param>
        /// <param name="strDesc"></param>
        public void PluginListBuilderAddMediaTypeToLastPlugin(IntPtr ptrBuilder, string strName, string strDesc)
        {
            IntPtr ptrName = strName.StrToUtf8Ptr();
            IntPtr PtrDesc = strDesc.StrToUtf8Ptr();

            QS_API.qsPluginListBuilderAddMediaTypeToLastPlugin(ptrBuilder, ptrName, PtrDesc);
        }

        /// <summary>
        /// 插件相关，需在qsGetPluginListCallback中调用
        /// </summary>
        /// <param name="ptrBuilder"></param>
        /// <param name="strFile"></param>
        public void PluginListBuilderAddFileExtensionToLastMediaType(IntPtr ptrBuilder, string strFile)
        {
            IntPtr PtrFile = strFile.StrToUtf8Ptr();

            QS_API.qsPluginListBuilderAddFileExtensionToLastMediaType(ptrBuilder, PtrFile);
        }

        /// <summary>
        /// 下载相关，需在qsDownloadCallback中调用
        /// </summary>
        /// <param name="strUrl"></param>
        /// <returns></returns>
        public bool PopupDownloadMgr(string strUrl)
        {
            int iRet = QS_API.qsPopupDownloadMgr(m_WebView, strUrl, IntPtr.Zero);

            return iRet == 1 ? true : false;
        }

        /// <summary>
        /// 下载相关，需在qsDownloadCallback中调用
        /// </summary>
        /// <param name="iContentLength"></param>
        /// <param name="strUrl"></param>
        /// <param name="strMime"></param>
        /// <param name="strDisposition"></param>
        /// <param name="ptrJob"></param>
        /// <param name="dataBind"></param>
        /// <param name="callbackBind"></param>
        /// <returns></returns>
        public qsDownloadOpt PopupDialogAndDownload(uint iContentLength, string strUrl, string strMime,
            string strDisposition, IntPtr ptrJob, qsNetJobDataBind dataBind, qsDownloadBind callbackBind)
        {
            return QS_API.qsPopupDialogAndDownload(m_WebView, IntPtr.Zero, iContentLength, strUrl, strMime,
                strDisposition, ptrJob, ref dataBind, ref callbackBind);
        }

        /// <summary>
        /// 下载相关，需在qsDownloadCallback中调用
        /// </summary>
        /// <param name="strPath"></param>
        /// <param name="iContentLength"></param>
        /// <param name="strUrl"></param>
        /// <param name="strMime"></param>
        /// <param name="strDisposition"></param>
        /// <param name="ptrJob"></param>
        /// <param name="dataBind"></param>
        /// <param name="callbackBind"></param>
        /// <returns></returns>
        public qsDownloadOpt DownloadByPath(string strPath, uint iContentLength, string strUrl, string strMime,
            string strDisposition, IntPtr ptrJob, qsNetJobDataBind dataBind, qsDownloadBind callbackBind)
        {
            IntPtr ptrPath = strPath.StrToUnicodePtr();
            return QS_API.qsDownloadByPath(m_WebView, IntPtr.Zero, ptrPath, iContentLength, strUrl, strMime,
                strDisposition, ptrJob, ref dataBind, ref callbackBind);
        }

        /// <summary>
        /// 获取pdf页面数据，结果在m_qsGetPdfPageDataCallback回调中取
        /// </summary>
        public void GetPdfPageData()
        {
            QS_API.qsGetPdfPageData(m_WebView, m_qsGetPdfPageDataCallback, IntPtr.Zero);
        }

        /// <summary>
        /// 截屏
        /// </summary>
        /// <param name="settings"></param>
        public void UtilScreenshot(qsScreenshotSettings settings)
        {
            QS_API.qsUtilScreenshot(m_WebView, ref settings, m_qsScreenshotCallback, IntPtr.Zero);
        }

        /// <summary>
        /// 打印pdf
        /// </summary>
        /// <param name="ptrFrameId"></param>
        /// <param name="settings"></param>
        public void UtilPrintToPdf(IntPtr ptrFrameId, qsPrintSettings settings)
        {
            QS_API.qsUtilPrintToPdf(m_WebView, ptrFrameId, ref settings, m_qsPrintPdfDataCallback, IntPtr.Zero);
        }

        /// <summary>
        /// 打印位图
        /// </summary>
        /// <param name="ptrFrameId"></param>
        /// <param name="settings"></param>
        public void UtilPrintToBitmap(IntPtr ptrFrameId, qsScreenshotSettings settings)
        {
            QS_API.qsUtilPrintToBitmap(m_WebView, ptrFrameId, ref settings, m_qsPrintBitmapCallback, IntPtr.Zero);
        }

        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="ptrFrameId"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        public bool UtilPrint(IntPtr ptrFrameId, qsPrintSettings settings)
        {
            int iRet = QS_API.qsUtilPrint(m_WebView, ptrFrameId, ref settings);
            return iRet == 1 ? true : false;
        }

        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="strDefaultPath"></param>
        /// <returns></returns>
        public bool UtilIsRegistered(string strDefaultPath)
        {
            IntPtr ptrDefaultPath = strDefaultPath.StrToUnicodePtr();
            int iRet = QS_API.qsUtilIsRegistered(ptrDefaultPath);

            return iRet == 1 ? true : false;
        }

        /// <summary>
        /// 创建请求码
        /// </summary>
        /// <param name="strRegisterInfo"></param>
        /// <returns></returns>
        public string UtilCreateRequestCode(string strRegisterInfo)
        {
            return QS_API.qsUtilCreateRequestCode(strRegisterInfo);
        }

        /// <summary>
        /// 从标记获取内容，结果在m_qsGetContentAsMarkupCallback中取
        /// </summary>
        /// <param name="frameHandle"></param>
        public void GetContentAsMarkup(IntPtr frameHandle)
        {
            QS_API.qsGetContentAsMarkup(m_WebView, m_qsGetContentAsMarkupCallback, IntPtr.Zero, frameHandle);
        }

        /// <summary>
        /// 获取网站logo
        /// </summary>
        public void GetFavicon()
        {
            QS_API.qsOnNetGetFavicon(m_WebView, m_qsNetGetFaviconCallback, IntPtr.Zero);
        }

        #endregion
    }
}
